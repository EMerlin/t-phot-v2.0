from sextutils import rw_catalog
import numpy as N
import os, sys


class tphot_catalog(rw_catalog):
    """ Customized catalog class with some tphot-specifics"""

    def __init__(self,fname,magzp=None,snlim=3):
        rw_catalog.__init__(self,fname)
        self._negflux=((self.fitqty <= 0))
        self._posflux=((self.fitqty > 0))
        if "nan" in str(self.fitqty):
            self.fitqty=1.e20
            #self.addcolumn('sn',self.fitqty/self.fitquerr)
        if magzp is not None:
            self._magzp=magzp
            mag=-2.5*N.log10(self.fitqty)+self._magzp
            mag[self._negflux]=-2.5*N.log10(snlim*self.fitquerr)+self._magzp
            self.addcolumn('mag',mag)

    def addcovar(self,cdict):
        self.addemptycolumn('MaxCvId','Int32')
        self.addemptycolumn('MaxCvRatio','Float32')
        for i in self:
            mycell=cdict[self.cell[i]]
            self.maxcvratio[i], self.maxcvid[i] = mycell.covarworst(self.objectid[i])
            
                                                                  
    def addnndist(self):
        self.addemptycolumn('nndist','Float32')
        for i in self:
            dx=self.x-self.x[i]
            dy=self.y-self.y[i]
            dr=N.sqrt(dx*dx + dy*dy)
            distsort=dr.argsort()
            self.nndist[i]=dr[distsort[1]]
