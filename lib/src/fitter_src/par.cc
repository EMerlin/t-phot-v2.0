#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "fitsio.h"
#include "nrutil.h"
#include "define.h"
#include "par.h"


Par::Par(int s) {
  data = dvector(1,s);
  usedata = ivector(1,s);
  for (int i = 1; i<=s; i++) {
    data[i] = 1.0;  // Added this initialization
    usedata[i] = 1;
  }
  size = s;
}

Par::~Par() {
  free_dvector(data,1,size);
  free_ivector(usedata,1,size);
}

Par::Par(double dat[], int idat[], int s) {
  data = dvector(1,s);
  usedata = ivector(1,s);
  size = s;
  for (int i=1;i<=size;i++) {
    data[i] = dat[i];
    usedata[i] = idat[i];
  }
}
