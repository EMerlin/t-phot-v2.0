#include <math.h>
#include "box.h"

#include <iostream>
using namespace std;
// A simple class to facilitate manipulating ranges.
// Note that lx,ly,ux,uy are *pixel indices*
// and they start counting from zero.
// Thus a 10x10 box will have corners 0,9,0,9, and npix will be 100.

Box::Box() {
    npix = 0;
}

void Box::UpdateCenter() {
    cx = (float)lx + (this->Xdim()-1)/2.;
    cy = (float)ly + (this->Ydim()-1)/2.;
}
Box::Box(long x1, long y1, long x2, long y2) {
    lx = x1; ly = y1;
    ux = x2; uy = y2;
    npix = (ux-lx+1)*(uy-ly+1);
    this->UpdateCenter();
}

long Box::Npix() { return npix; }
float Box::Cx() {return cx; }
float Box::Cy() {return cy; }

void Box::Set(long x1, long y1, long x2, long y2) {
    lx = x1; ly = y1; 
    ux = x2; uy = y2;
    npix = (ux-lx+1)*(uy-ly+1);
    this->UpdateCenter();

}

void Box::Move(long xdelta, long ydelta) {
  lx = lx + xdelta;
  ux = ux + xdelta;
  ly = ly + ydelta;
  uy = uy + ydelta;
  this->UpdateCenter();
}

long Box::Xdim() { return ux-lx+1; }
long Box::Ydim() { return uy-ly+1; }
long Box::Lx() { return lx; }
long Box::Ly() { return ly; }

float Box::Xpos(float xx) { return xx-lx; }
float Box::Ypos(float yy) { return yy-ly; }

float Box::RadialDistance(float xx, float yy) {
  float r2;
  float localx, localy;
  r2 = pow((xx-cx),2) + pow((yy-cy),2);
  return sqrt(r2);
}
  
bool Box::Contains(float xx, float yy) {
  return (xx >= lx && xx <= ux && yy >= ly && yy <= uy); }

bool Box::Contains(Box thatbox) {
  return (this->Contains(thatbox.lx, thatbox.ly) &&
	  this->Contains(thatbox.ux, thatbox.uy) ); 
}

bool Box::Contains(int oxd, int oyd, int oxu, int oyu) {
  return (this->Contains((float)oxd, (float)oyd) &&
	  this->Contains((float)oxu, (float)oyu) ); 
}

bool Box::Overlaps(Box thatbox) {
  // NB: This means a partial overlap.
  if ( this->Contains(thatbox) || thatbox.Contains(*this) )
    { return 0; }
  else {
    return (this->Contains(thatbox.lx, thatbox.ly) ||
	  this->Contains(thatbox.ux, thatbox.ly) ||
	  this->Contains(thatbox.lx, thatbox.uy) ||
	  this->Contains(thatbox.ux, thatbox.uy) || 
	  thatbox.Contains(lx,ly) || 
	  thatbox.Contains(ux,ly) || 
	  thatbox.Contains(lx,uy) || 
	  thatbox.Contains(ux,uy) ) ;
  }
}

void Box::GrowsToHold(Box thatbox) {
  if (lx > thatbox.lx) lx=thatbox.lx;
  if (ly > thatbox.ly) ly=thatbox.ly;
  if (ux < thatbox.ux) ux=thatbox.ux;
  if (uy < thatbox.uy) uy=thatbox.uy;
  npix = (ux-lx+1)*(uy-ly+1);
  this->UpdateCenter();
}

void Box::GrowsToHold(int oxd, int oyd, int oxu, int oyu) {
  if (lx > oxd) lx=oxd;
  if (ly > oyd) ly=oyd;
  if (ux < oxu) ux=oxu;
  if (uy < oyu) uy=oyu;
  npix = (ux-lx+1)*(uy-ly+1);
  this->UpdateCenter();
}

void Box::ShrinksToFit(Box thatbox) {
  if (lx < thatbox.lx) lx=thatbox.lx;
  if (ly < thatbox.ly) ly=thatbox.ly;
  if (ux > thatbox.ux) ux=thatbox.ux;
  if (uy > thatbox.uy) uy=thatbox.uy;
  npix = (ux-lx+1)*(uy-ly+1);
  this->UpdateCenter();
}

bool Box::IsInside(Box thatbox) {
  //cout<<"---"<<lx<<" "<<ly<<" "<<ux<<" "<<uy<<" "<<thatbox.Contains(lx, ly)<<" "<<thatbox.Contains(ux,uy)<<endl;
  //cout<<thatbox.lx<<" "<<thatbox.ly<<" "<<thatbox.ux<<" "<<thatbox.uy <<endl;
  return (thatbox.Contains(lx, ly) && thatbox.Contains(ux,uy) );
}

void Box::operator=(Box thatbox) {
  lx=thatbox.lx;
  ly=thatbox.ly;
  ux=thatbox.ux;
  uy=thatbox.uy;
  npix=thatbox.npix;
  cx=thatbox.cx;
  cy=thatbox.cy;
}

bool Box::operator==(Box other) {
  return (lx == other.lx &&
	  ly == other.ly &&
	  ux == other.ux &&
	  uy == other.uy);
}

Box::~Box() {
  npix=0;
  lx=ly=ux=uy=0;
}

