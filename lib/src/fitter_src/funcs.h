/********************************************************************
 *
 * Header file containing the prototypes for the fitting routines
 * and the numerical recipes algorithms for SVD and covariance matrix
 * computation
 * 
 * Last Edited: 05 DEC 2012
 *
 * Added 29 Jan 1999
 *  setGroup now uses a distance vs radius of object test to determine 
 *  if adjacent objects belong in the same group
 *
 * Added 6 Feb 2001
 *  incorporated possibility of using NR LFIT in addition to NR SVD
 *
 ********************************************************************/

#ifndef FUNCS_H
#define FUNCS_H

/********************************************************************
 *
 * IMPORTANT: the following line determines which chi2-minimization 
 * routine to use for the fitting.  If LFIT is defined, it will use
 * a least-squares fitting technique.  Otherwise, it will revert to the
 * more robust singular value decomposition by default.
 *
 * In general, use SVD fitting (so comment out the define LFIT command).
 * However, if you have long object-chains in practice, it will probably
 * be necessary to use LFIT.
 *
 ********************************************************************/
//#include "tlibrary.h"

//#define LFIT


class Funcs {
public:

  void fitFits(Fits *, Fits *, Fits *, Fits *,int,Fits *,char[],char[],char[],
	       int,char[],char[],float,float,int,char[],char[],int,
	       int,char[],int, int, int , int *);

  void walkGrid(char*, char*, char*, char*, 
		double*, double*, long*, long*,
		int, int, int, 
		char*, char*, Grid *, 
                float, float, double,
		int, float, int, 
		int, int, int, char*, 
		int, int, int,
		int, int, int, int, int, int, int,
		float, int, int, char*, int*, 
		int, int*);

  void walkGrid_coo(char*, char*, char*, char*, 
		    double*, double*, long*, long*,
		    int, int, int,
		    char*, char*, char*, Grid*, 
		    float, float, double, 
		    int, float, int, 
		    int, int, int, char*, 
		    int, int, int, int, int,
		    int, int, int, int, int, int, int,
		    float, int, int, char*, int*, int, 
		    int, float*, int, int*);

  int *cellmask(float, int, long*, double**, int, int, int&);

  double getmaxcomp(double**, int, int);

  void setGroup(gNode *,List *,Fits *,Fits *,Grid *,int,int,int,int *,
		int *,float, long, long, long, long, int, int *);
  void setBounds(int *,int *,int *,int *,Fits *,int,float,float,int);

  double sq(double x);
  double pythag(double,double);
  double gammln(double);
  double gammq(double, double);

  void gcf(double *, double, double, double *);
  void gser(double *, double, double, double *);

  void sprsin(double **, int, double, unsigned long, double [], unsigned long []); 
  void svdfit(double *[], double [], double [], int, double [],int,double **,
	      double **,double [],double *,
	      void (*funcs)(double [],double [],int,int), int);
  void svdvar(double **, int, double [], double **);
  void svbksb(double **,double [],double **,int,int,double [],double []);
  void svdcmp(double **,int,int,double [],double **);

  void linbcg(double [], unsigned long [], unsigned long, double [], double [], int, double,
	      int, int *, double *);
  void ludcmp(double **, int, int *, double *);
  void lubksb(double **, int, int *, double []);
  
  void choldc(double **a, int n, double p[]);
  void cholsl(double **a, int n, double p[], double b[], double x[]);
  
  void lfit(double *[], double [], double [], int, double [], int [],
	    int, double **, double *, 
	    void (*funcs)(double [], double [], int, int), int);
  void covsrt(double **, int, int [], int);
  void gaussj(double **, int, double **, int);

  void matrix_2_colmaj(double **, double [], int, int);
  void colmaj_2_matrix(double [], double **, int, int);
  
  void print_matrix(double [], int, int);

};

#endif
