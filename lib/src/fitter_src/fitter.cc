/*********************************************************************
 * main.cc
 * Created 1 Feb 1999 - Papovich
 *   This program matches the given isophotes for "perfect"
 *   resolution to the same image with poorer resolution. 
 * * *
 * Major revision: Oct 25 2002 - Laidler
 *  Revised architecture for efficiency. Template images are now
 * convolved and resampled to low resolution in an earlier processing
 * step. TFIT now reads this library of templates, instead of producing
 * them on the fly. 
 *
 * * *
 * Revision to add new parameters - Laidler
 *   Dither parameter to control whether cell pattern is dithered
 *   Background subtraction parameter to subtract background in memory.
 *
 * * *
 * Revision Mar 23 2006 - Laidler
 *     Version renumbering in define.h in preparation for first
 * public release: Current version redefined as version 1.0beta.
 * * *
 * Revision Dec 20 2007 - Laidler
 *     Version renumbering in define.h for 1.0 release.
 *
 * * * * * *
 * Major Revision 2013/14/15 - Merlin
 *     Major modifications and inclusion in the T-PHOT package.
 *********************************************************************/

#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include "fitsio.h"
#include "nrutil.h"
#include "define.h"
#include "fits.h"
#include "structs.h"
#include "funcs.h"
#include "tphot.h"
#include <vector>

#define XGRID_SIZE LRW / 10 
#define YGRID_SIZE LRH / 10 

#define	QCALLOC(ptr, typ, nel)  ptr = (typ *)calloc((size_t)(nel),sizeof(typ))
#define	QMALLOC(ptr, typ, nel)  ptr = (typ *)malloc((size_t)(nel)*sizeof(typ))
#define	QFREE(ptr) {free(ptr); ptr = NULL;}
long Width, Height;
long MeasXoff, MeasYoff;
long MeasXmin, MeasXmax, MeasYmin, MeasYmax;
long MeasWidth, MeasHeight;
long OverlapWidth, OverlapHeight;
uchar Offset;

int main(int argc, char *argv[]) {

  /*********************************************************************
   *
   * DEFINE & DECLARE VARIABLES
   *
   *********************************************************************/
  fitsfile *chkfitsopen (char* , char);
  int 
    i, j,
    MEPROC=1,
    status=0,
    line=0,
    nosub=0,
    MAX_FLAG=0,
    NPROC=1,
    USE_ERRORS=FALSE,
    USE_CELLMASK=FALSE,
    DITHERCELL=FALSE,
    CREATE_TEMPLATE=FALSE,
    CHECK_TEMPLATES=FALSE,
    EXPORT_COV_INFO=FALSE,
    FIT_BACKGROUND=0,
    SETGROUP=TRUE,
    CLIP=FALSE,
    XBUFFERSIZE=DEFAULT_BUFFER,
    YBUFFERSIZE=DEFAULT_BUFFER,
    FLAGDISTANCE=DEFAULT_FLAG_DIST,
    linsyssolver=1,
    nc=1, nr=1, nc1,
    xxx=1, yyy=1,
    nreg=-1,
    dxopt, dyopt,
    xrange, yrange,
    naper=0,
    nsourcesopt=2000; //10000;
  float 
    RMS_CONSTANT=1.0,
    BG_CONSTANT=0.0,
    secs,
    ThresholdFactor,
    ZPHR=0.0, ZPLR=0.0,
    MASK_FLOOR=0.0;
  double facZP=1.0;

  time_t 
    start=time(NULL),
    end;
  Fits 
    *Image, 
    *thisone,
    *FlagMap,
    *WeightMap;  

  long 
    naxes[2] = {1,1},
    LRW=0, LRH=0;
  fitsfile 
    *fimage, 
    *ferr,
    *fflag,
    *fseg;
  Funcs masterCode;   
  ifstream parin;
  char *wid,*hei;
  Grid * BigGrid;
  long * objlist;
  double *pixels, *pixels_rms;
  int *blendedpriors;
  long *pixels_flag, *pixels_seg;
  long fpixel[2] = {1,1};
  char *token;
  float *APER;
  

  char * CHOICE;
  char * SexFile; 
  char * LoResFits;
  char * TemplateFits;
  char * WeightFits; 
  char * FlagFits;
  char * SegFits;
  char * ConSegFits; 
  char * GrpFits; 
  char * growthfile; 
  char * convfile;
  char * outfile; 
  char * cellfile;
  char * aperfile;
  char * TemplateImageFile; 
  char * TemplateDir;
  char * ResidualImageFile; 
  char * CovarianceFile;
  char * errchoice;
  char * FluxOrderedCatalog;
  char * convo; 
  
  gNode *item;
  void printTime(float);


  LoResFits=argv[1];
  SexFile=argv[2];
  TemplateDir=argv[3];
  outfile=argv[4];
  cellfile=argv[5];
  SegFits=argv[6];
  errchoice=argv[7];
  WeightFits=argv[8];
  RMS_CONSTANT=atof(argv[9]);
  if (argv[10][0]=='T')
    {USE_CELLMASK=TRUE;}
  else
    {USE_CELLMASK=FALSE;}
  MASK_FLOOR=atof(argv[11]);
  if (argv[12][0]=='T')
    {DITHERCELL=TRUE;}
  else
    {DITHERCELL=FALSE;}
  FlagFits=argv[13];
  MAX_FLAG=atoi(argv[14]);
  if (argv[15][0]=='T')
    {FIT_BACKGROUND=1;}
  else
    {FIT_BACKGROUND=0;}
  BG_CONSTANT=atof(argv[16]);
  if (argv[17][0]=='T')
    {EXPORT_COV_INFO=TRUE;}
  else
    {EXPORT_COV_INFO=FALSE;}
  CovarianceFile=argv[18];
  XBUFFERSIZE=atoi(argv[19]);
  YBUFFERSIZE=atoi(argv[20]);
  FLAGDISTANCE=atoi(argv[21]);
  ThresholdFactor=atof(argv[22]);
  if (argv[23][0]=='L')
    {linsyssolver=1;}
  else if (argv[23][0]=='C')
    {linsyssolver=2;}
  else if (argv[23][0]=='I')
    {linsyssolver=3;}
  if (argv[24][0]=='T')
    {CLIP=TRUE;}
  else
    {CLIP=FALSE;}
  NPROC=atoi(argv[25]);
  ZPHR=atof(argv[26]);
  ZPLR=atof(argv[27]);
  FluxOrderedCatalog=argv[28];
  MEPROC=atoi(argv[29]);
  if (argc>30)
    {
      aperfile=argv[30];
      naper=atoi(argv[31]);
      APER=(float *)malloc(naper*sizeof(float));
      int ap=0;
      while(ap<naper)
	{
	  APER[ap]=atof(argv[31+ap]);
	  ap+=1;	    
	}
    }
  else
    {
      aperfile="";
    }
  
  cout << "ME: " << MEPROC << endl;
  /*********************************************************************
   *
   * List of input files and parameters: 
   *  (lines starting with '#' in the param fil
   *  are ignored).
   *  --------------------
   *  SexFile : the output list from SExtractor (or something similar)
   *  LoResFits : the Low Resolution image (.fits) image to be chi2 fitted
   *  Templatefits : the Hi-Res image (.fits) resolution fits image
   *  WeightFits : The weight map for the LoRes image (.fits) image
   *  SegImgFits : The segmentation map (.fits) image of the 
   *               Hi-res image (PIXTYPE=INT)
   *  outfile : output file -- will contain fluxes, scaling factors, etc.
   *  convfile : the input file holding the convolution kernel 
   *  USE_ERRORS: if true, then the routine uses the weight map -- 
   *      else no weight used and approximate errors will be given for the 
   *      final fluxes.
    *  RMS_CONSTANT:  A constant which gives the errors from the weight map
   *      via the relation:  err = RMS_CONSTANT / weight
   *  EXPORT_COVAR_INFO: The program will write all the covariance matrix in
   *      formation for each group fitted to a file, where the file is
   *	 specified by:
   *  COVARIANCE_FILE: (see above)
   *
   *********************************************************************/
  
  /*********************************************************************
   *
   * READ IN PARAMETER FILE
   * (Reading in flags from command line is no longer supported.)
   *********************************************************************/

  /*
  parin.open(argv[1]);
  if (!parin) {
    cout << "unable to find param file\n";
    return 1;
  }
  cout << "Reading parameter file: " << argv[1] << endl;
  parin.setf(ios::uppercase);
  int ap=0, naper=0;
  float * APER;
  while(parin) {
    parin.getline(CHOICE,LINE_SIZE); line++;
    if (!strcmp(CHOICE,"")) break;
    while (!strncmp(CHOICE,"#",1)) {
      parin.getline(CHOICE,LINE_SIZE); line++;}

    token = strtok(CHOICE,",;\t \n"); 
    if (!strncmp(token,"OBJECT",6)) 
      strcpy(SexFile,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"LOW_RES",7)) 
      strcpy(LoResFits,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"FLAG_MAP",8)) 
      strcpy(FlagFits,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"SEGM",4)) 
      strcpy(SegFits,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"OUTPUT",6)) 
      strcpy(outfile,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"CELLFILE",8)) 
      strcpy(cellfile,token=strtok('\0',",;\t "));

    else if (!strncmp(token,"USE_ERR",7)) 
      strcpy(errchoice,token=strtok('\0',":,;\t "));
   else if (!strncmp(token,"ERR_MAP",7)) 
      strcpy(WeightFits,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"RMS_CON",7)) 
      RMS_CONSTANT = atof(token=strtok('\0',",;\t "));

    else if (!strncmp(token,"FIT_BACK",8)) {
      if (atoi(token=strtok('\0',",;\t ")) == 1 || 
	  !strncmp(token,"T",1) ||
	  !strncmp(token,"Y",1)) FIT_BACKGROUND = 1;
      else FIT_BACKGROUND = 0;
    }
    else if (!strncmp(token,"CLIP",4)) {
      if (atoi(token=strtok('\0',",;\t ")) == 1 || 
	  !strncmp(token,"T",1) ||
	  !strncmp(token,"Y",1)) CLIP = TRUE;
      else CLIP = FALSE;
    }

    else if (!strncmp(token,"BG_CON",6)) 
      BG_CONSTANT = atof(token=strtok('\0',",;\t "));

    else if (!strncmp(token,"CELLMASK",8)) {
      if (atoi(token=strtok('\0',",;\t ")) == 1 || 
	  !strncmp(token,"T",1) ||
	  !strncmp(token,"Y",1)) USE_CELLMASK = TRUE;
      else USE_CELLMASK = FALSE;
    }
    else if (!strncmp(token,"FLOOR",5)) 
      MASK_FLOOR = atof(token=strtok('\0',",;\t "));

    else if (!strncmp(token,"DITHERCELL",10)) {
      if (atoi(token=strtok('\0',",;\t ")) == 1 || 
	  !strncmp(token,"T",1) ||
	  !strncmp(token,"Y",1)) DITHERCELL = TRUE;
      else DITHERCELL = FALSE;
    }

    else if (!strncmp(token,"MAX_FLAG",8)) 
      MAX_FLAG = atoi(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"NPROC",5)) 
      NPROC = atoi(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"TEMPLATE_",9)) 
      strcpy(TemplateDir,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"EXPORT_",7))  {
      if (atoi(token=strtok('\0',",;\t ")) == 1 || 
	  !strncmp(token,"T",1) ||
	  !strncmp(token,"Y",1))  EXPORT_COV_INFO=TRUE;
      else EXPORT_COV_INFO = FALSE;
    } 
    else if (!strncmp(token,"THRESH",6)) 
      ThresholdFactor = atof(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"LINSYS",6))  {
      token=strtok('\0',",;\t ");
      if (!strncmp(token,"LU",2)) {
	cout<<"Using LU method"<<endl;
	linsyssolver=1;
      }
      else if (!strncmp(token,"CHO",3)) {
	cout<<"Using Cholesky method"<<endl;
	linsyssolver=2;
      }
      else if (!strncmp(token,"IBG",3)){
	cout<<"Using IBG method"<<endl;
	linsyssolver=3;
      } 
      else {
	cout<<"Undefined linear system solver;"<<endl;
	cout<<"Using default (LU) method"<<endl;	
	linsyssolver=1;	
      }
    }
    else if (!strncmp(token,"COVAR",5)) 
      strcpy(CovarianceFile,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"XBUFFER",7))
      XBUFFERSIZE=atoi(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"YBUFFER",7))
      YBUFFERSIZE=atoi(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"ZPHR",4)) 
      ZPHR = atof(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"ZPLR",4)) 
      ZPLR = atof(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"FLAG_DIST",9))
      FLAGDISTANCE=atoi(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"MEPROC",6))
      MEPROC=atoi(token=strtok('\0',",;\t "));
    else if (!strncmp(token,"FLUXORDERED",11)) 
      strcpy(FluxOrderedCatalog,token=strtok('\0',",;\t "));
    else if (!strncmp(token,"NAPER",5))
      {
	naper=atoi(token=strtok('\0',",;\t "));
	APER=(float *)malloc(naper*sizeof(float));
      }
    else if (!strncmp(token,"APE",3))
      {
	APER[ap]=atof(token=strtok('\0',",;\t ")); // diameters!
	ap+=1;
      }
    
    else {
      cout << "% KEYWORD: " << token << " unknown in line: ";
      cout << line << endl;
    }
  }
  parin.close();
  */

  /*********************************************************************
   *
   * READ IN FITS FILES
   *
   *********************************************************************/

  // Read measure image
  fimage=chkfitsopen(LoResFits,'i');
  QMALLOC(pixels, double, MeasWidth*MeasHeight);
  fits_read_pix(fimage, TDOUBLE, fpixel, MeasWidth*MeasHeight, NULL,
		pixels, NULL, &status); 
  LRW=MeasWidth;
  LRH=MeasHeight;
  if (BG_CONSTANT!=0.0) { // subtract constant background
    for (i=0;i<Width*Height;i++) pixels[i]-=BG_CONSTANT;
  }
  
  fits_close_file(fimage, &status); 

  cout << "Reading objects catalog: " << FluxOrderedCatalog << endl;
  //cout << "Reading objects catalog: " << SexFile << endl;
  /* Populate the BigGrid from the lowres templates catalog
     This function builds a Grid (structs_grid.cc) putting objects 
     from template catalog into a matrix of range 
     xsize/XGRID_SIZE = 10, ysize/YGRID_SIZE = 10 (fixed!!!) */
  BigGrid = new Grid(FluxOrderedCatalog, //SexFile,
		     (long)LRW,(long)LRH,(long)XGRID_SIZE,
		     (long)YGRID_SIZE); 
  
  BigGrid->list();  // Diagnostic output

  int nsources,ii;
  nsources=BigGrid->Count();
  long *ids;
  ids=(long *)malloc(nsources*sizeof(long));
  ids=BigGrid->objlist(); // list of ids
  //for (ii=0;ii<nsources;ii++) {cout << ids[ii] << " ";} cout << endl; 
  cout << "Nsources: " << nsources << endl;

  // Parallel work: find out the region of the images
  // this CPU is dealing with
    if (NPROC>1){
      nc=int(floor(sqrt(NPROC)));
      for (i=0;i=nc-1;i++){
	nc1=nc-i;
	if (NPROC%nc1==0) {
	  nc=nc1;
	  break;
	}
      }
      nr=NPROC/nc;
      if ((nc>nr) && (LRW<LRH)){
	nc1=nc;
	nc=nr;
	nr=nc1;}
      else if ((nr>nc) && (LRH < LRW)){
	nc1=nc;
	nc=nr;
	nr=nc1;}
      if (nc*nr != NPROC) {
	cout << "TERROR in dividing image for parallel work" << endl;
	exit(-1);
      }
      /// xxx and yyy are the positions of the local CPU region
      if (MEPROC%nc==0){
	xxx=nc;
	yyy=MEPROC/nc;}
      else{
	xxx=MEPROC%nc;
	yyy=MEPROC/nc+1;}
    }

  xrange=LRW/nc;
  yrange=LRH/nr;
  
  // Estimate optimized size of regions for nsourcesopt objects
  nreg=(floor)(nsources/nsourcesopt)+1;
  dxopt=MAX(int(floor(sqrt(nreg))),1);
  /*
  for (i=0;i=dxopt-1;i++){
    nc1=dxopt-i;
    if (nreg%nc1==0) {
      dxopt=nc1;
      break;
    }
  }
  */
  dyopt=MAX(nreg/dxopt,1);
  dxopt=(int)(xrange/dxopt);
  dyopt=(int)(yrange/dyopt);
  dxopt=(int)sqrt(dxopt*dyopt);
  dyopt=dxopt;
  cout << "Optimized cell size:"<<dxopt<<","<<dyopt<<endl; 
  if (XBUFFERSIZE < 0 || YBUFFERSIZE < 0) {
    if (XBUFFERSIZE < -1 || YBUFFERSIZE < -1) {
      cout << "Fitting on optimized regions" << endl;
      XBUFFERSIZE = dxopt;
      YBUFFERSIZE = dyopt;
      FLAGDISTANCE=(int)MAX(XBUFFERSIZE,YBUFFERSIZE)/5;
      cout << "Optimized single cell fitting: X, Y = " << XBUFFERSIZE << ", " << YBUFFERSIZE << "; overlapping region = " << FLAGDISTANCE << endl;
    }
    else{
      XBUFFERSIZE = xrange;
      YBUFFERSIZE = yrange;
      DITHERCELL = 0;
      cout << "Single cell fitting: X, Y = " << XBUFFERSIZE << ", " << YBUFFERSIZE << endl;
    }
  }
  else if (XBUFFERSIZE == 0 || YBUFFERSIZE == 0) {
    cout << "Cells on objects fitting "<<endl;
  }
  else {
    cout << "Cells size: X, Y = " << XBUFFERSIZE << ", " << YBUFFERSIZE << "; overlap region = " << FLAGDISTANCE << endl;
  }  

  int nblended=0;
  if (fseg=chkfitsopen(SegFits,'r')) {
    // Read in Segmentation image and build matrix for priors blending
    QMALLOC(blendedpriors, int, nsources);
    blendedpriors[0]=0;
    for (i=0;i<nsources;i++) blendedpriors[i]=0;
    
    QMALLOC(pixels_seg, long, Width*Height);
    fits_read_pix(fseg, TLONG, fpixel, Width*Height, NULL,
 		  pixels_seg, NULL, &status); 
    fits_close_file(fseg, &status);
       
    int id, idj, ii, jj, ins;
    ii=0;
    for (i=0;i<Width-1;i++){
      for (j=0;j<Height-1;j++){
	id=pixels_seg[Width*j+i];
	if (id>0){
	  idj=pixels_seg[Width*(j+1)+i]; // check pixel below
	  if (idj!=id && idj>0){
	    ins=1; // insert id
	    for (jj=0;jj<ii;jj++){
	      if (blendedpriors[jj]==id) 
		{ins=0;break;}}
	    for (jj=0;jj<nsources;jj++){
	      if (ids[jj]==id)
		{break;}}
	    if (jj==nsources && ids[jj]!=id) ins=0;
	    if (ins) {
	      blendedpriors[ii]=id;
	      ii+=1;
	    }
	    ins=1; // insert idj
	    for (jj=0;jj<ii;jj++){
	      if (blendedpriors[jj]==idj) 
		{ins=0;break;}}
	    for (jj=0;jj<nsources;jj++){
	      if (ids[jj]==idj)
		{break;}}
	    if (jj==nsources && ids[jj]!=idj) ins=0;
	    if (ins) {
	      blendedpriors[ii]=idj;
	      ii+=1;
	    }
	  }
	  idj=pixels_seg[Width*j+(i+1)]; // check pixel on the right
	  if (idj!=id && idj>0){
	    ins=1; // insert id
	    for (jj=0;jj<ii;jj++){
	      if (blendedpriors[jj]==id) 
		{ins=0;break;}}
	    for (jj=0;jj<nsources;jj++){
	      if (ids[jj]==id)
		{break;}}
	    if (jj==nsources && ids[jj]!=id) ins=0; 
	    if (ins) {
	      blendedpriors[ii]=id;
	      ii+=1;
	    }
	    ins=1; // insert idj
	    for (jj=0;jj<ii;jj++){
	      if (blendedpriors[jj]==idj) 
		{ins=0;break;}}
	    for (jj=0;jj<nsources;jj++){
	      if (ids[jj]==idj)
		{break;}}
	    if (jj==nsources && ids[jj]!=idj) ins=0;
	    if (ins) {
	      blendedpriors[ii]=idj;
	      ii+=1;
	    }
	  }
	}
      }
    }
    nblended=ii;
    cout << "Found " << nblended << " blended priors" << endl;
  }
  else{
    fprintf(stderr,"No Segmentation image found. Skipping and proceeding to fit\n");
    QMALLOC(pixels_seg, long, 1);
    pixels_seg[0]=0.0;
    QMALLOC(blendedpriors, int, 1);
    blendedpriors[0]=-1;
  }

  if (!strncmp(errchoice,"WEIGHT",6) || !strncmp(errchoice,"RMS",3) || !strncmp(errchoice,"WHT",3)) {
    USE_ERRORS = TRUE;
    cout << "Reading error map ..." << endl;
    ferr=chkfitsopen(WeightFits,'R');
    QMALLOC(pixels_rms, double, MeasWidth*MeasHeight);
    fits_read_pix(ferr, TDOUBLE, fpixel, MeasWidth*MeasHeight, NULL,
		  pixels_rms, NULL, &status);
    fits_close_file(ferr, &status);

    // If weight map, need to transform to rms
    if (!strncmp(errchoice,"WEIGHT",6) || !strncmp(errchoice,"WHT",3)) {
      cout << " ... Converting weight map to RMS map via RMS constant = "  << RMS_CONSTANT << endl;
      for (i=0; i<MeasWidth; i++)
	for (j=0; j<MeasHeight; j++) {	  
	  if (pixels_rms[MeasWidth*j+i] == 0.0) {
	    pixels_rms[MeasWidth*j+i] = MAX_FLOAT;
	  } else {
	    pixels_rms[MeasWidth*j+i] = RMS_CONSTANT/sqrt(pixels_rms[MeasWidth*j+i]);
	  }
	}
    } else { cout << "     Using RMS map exactly as provided" << endl; }
  } else {
    WeightMap = new Fits(0, MeasWidth, 0, MeasHeight, TFLOAT, RMS_CONSTANT);
    if (!strncmp(errchoice,"NONE",4)){
      cout << "     Using uniform RMS map with value " << RMS_CONSTANT << endl;
    } else { 
      cout << "     Using uniform RMS map with value 1" << endl;
    }
    QMALLOC(pixels_rms, double, MeasWidth*MeasHeight);
    for (i=0;i<MeasWidth;i++) 
      for (j=0;j<MeasHeight;j++){
	pixels_rms[j*MeasWidth+i]=WeightMap->data[j][i];
      }
  }
  // Check that RMS map has good data
  for (i=0;i<MeasWidth;i++) 
    for (j=0;j<MeasHeight;j++){
      if (pixels_rms[j*MeasWidth+i]<=0.0e0)
	{
	  printf("TERROR: RMS map has value %g at X,Y %d %d\n",pixels_rms[j*MeasWidth+i],i,j);
	  printf("Aborting\n");
	  exit(1);
	}
    }
  
  
  if (!strncmp(FlagFits,"NONE",4)){
    cout << "Opening flag image..." << endl;
    fflag=chkfitsopen(FlagFits,'R');
    QMALLOC(pixels_flag, long, MeasWidth*MeasHeight);
    fits_read_pix(fflag, TLONG, fpixel, MeasWidth*MeasHeight, NULL,
		  pixels_flag, NULL, &status); 
    fits_close_file(fflag, &status); 
    
  } else {
    cout << "No flag image used; setting max_flag = 0 " <<endl;
    FlagMap = new Fits(0, MeasWidth, 0, MeasHeight, TLONG, 0);
    
    //FlagMap->newTitle("Null flag map");
    MAX_FLAG = 0;
    QMALLOC(pixels_flag, long, MeasWidth*MeasHeight);
    for (i=0;i<MeasWidth;i++) 
      for (j=0;j<MeasHeight;j++){
	pixels_flag[j*MeasWidth+i]=FlagMap->ldata[j][i];
      }   
    //delete FlagMap;
  }
  
  /*********************************************************************
   *
   * Start the main body of the code
   *
   *********************************************************************/
  
  cout << "Doing the fit. Please wait..." << endl;
  cout << "nc, nr, xxx, yyy, xrange, yrange: " << nc << ", " << nr 
       << ", " << xxx << ", " << yyy << ", "  <<xrange << ", "  << yrange << endl; 
  
  if (ZPHR>0.0 && ZPLR >0.0) {
    facZP=pow(10.,-0.4*(ZPHR-ZPLR));
  }
  cout<<"facZP " << facZP<<endl;
  
  if (XBUFFERSIZE == 0 || YBUFFERSIZE == 0) { 
    fprintf(stderr,"Beginning fit in Cells-On-Objects mode.\n");
    fprintf(stderr,"Please hold on...\n");
    //sprintf(aperfile,outfile);
    //strcat(aperfile,"_apertures");
    masterCode.walkGrid_coo(SexFile,TemplateDir,LoResFits,WeightFits,
			    pixels,pixels_rms,pixels_flag,pixels_seg,
			    MeasWidth,MeasHeight,USE_ERRORS,
			    outfile, cellfile, aperfile, BigGrid,
			    RMS_CONSTANT, BG_CONSTANT, facZP, 
			    USE_CELLMASK, MASK_FLOOR, DITHERCELL, 
			    MAX_FLAG, FIT_BACKGROUND,
			    EXPORT_COV_INFO,CovarianceFile,
			    XBUFFERSIZE,YBUFFERSIZE,FLAGDISTANCE,dxopt,dyopt,
			    nc,nr,xxx,yyy,xrange,yrange,MEPROC,
			    ThresholdFactor, linsyssolver, CLIP, 
			    FluxOrderedCatalog, blendedpriors, nblended,
			    naper,APER,nsourcesopt,&status);
  }
  else {
    if (XBUFFERSIZE == xrange && YBUFFERSIZE == yrange) {
      fprintf(stderr,"Beginning fit in SingleCell mode.\n");}
    else{
      if (nreg>=0) {
	fprintf(stderr,"Beginning fit in Optimized SingleCell mode.\n");}
      else{
	fprintf(stderr,"Beginning fit in GridCells mode.\n");}
    }
    fprintf(stderr,"Please hold on...\n");
    masterCode.walkGrid(SexFile,TemplateDir,LoResFits,WeightFits,
			pixels,pixels_rms,pixels_flag,pixels_seg,
			MeasWidth,MeasHeight,USE_ERRORS,
			outfile, cellfile, BigGrid,
			RMS_CONSTANT, BG_CONSTANT, facZP,
			USE_CELLMASK, MASK_FLOOR, DITHERCELL, 
			MAX_FLAG, FIT_BACKGROUND,
			EXPORT_COV_INFO,CovarianceFile,
			XBUFFERSIZE,YBUFFERSIZE,FLAGDISTANCE,
			nc,nr,xxx,yyy,xrange,yrange,MEPROC,
			ThresholdFactor, linsyssolver, CLIP, 
			FluxOrderedCatalog, blendedpriors, nblended,
			&status);
  }
  // Clean up & go home.
  
  QFREE(pixels_seg);
  if (naper) free(APER);

  end = time(NULL);
  secs = difftime(end,start);
  
  printTime(secs);
  cout << "Fitter Done... FITS status = " << status << endl;
  fprintf(stderr,"\n");
  printf("\n");
  return(status);
}

/*********************************************************************
 *
 * SUBROUTINES
 *
 *********************************************************************/

void printTime(float secs) {
  int hours=0,mins=0;
  if (secs >=60) {
    mins = (int) floor(secs) / 60;
    secs -= 60.0*(float)mins;
  }
  if (mins >=60) {
    hours = mins/60;
    mins -= 60*hours;
  }
  cout << "Runtime = ";
  if (hours) cout << hours << "h:" << mins << "m:" << secs <<"s" << endl;
  else if (mins) cout << mins << "m:" << secs << "s" << endl;
  else cout << secs << "s" << endl;
  return;
}

/* ------------------------------------------------------------------------------ */

fitsfile *chkfitsopen (char *filename, char type) {
  fitsfile *ptr;
  char keyword[FLEN_KEYWORD];
  char comment[FLEN_COMMENT];
  int status = 0;   /*  CFITSIO status value MUST be initialized to zero!  */
  int naxis;
  long naxes[2] = {1,1};
  double dblvalue;
  uchar CheckSize = 0;

  if(fits_open_file(&ptr, filename, READONLY, &status)) {
    cout << status << endl;
    fits_report_error(stderr, status);
    return(NULL);
  }

  /* Read dimensions */
  if(fits_get_img_dim(ptr, &naxis, &status)) {
    fits_report_error(stderr, status);
    return(NULL);
  }

  if (naxis > 2) {
    fprintf(stderr, "ERROR: %s has > 2 dimensions\n", filename);
    return(NULL);
  }

  /* Read size */
  if(fits_get_img_size(ptr, 2, naxes, &status)) {
    fits_report_error(stderr, status);
    return(NULL);  
  }  

  switch (type) {
  case 'i': // Measure image
    // Set offset default values
    MeasXoff = 0.0;
    MeasYoff = 0.0;
    
    // Set measure image size
    MeasWidth = naxes[0];
    MeasHeight = naxes[1];
    
    // Search for offset keywords
    sprintf(keyword, "CPHXOFF");
    fits_read_key_dbl(ptr, keyword, &dblvalue, comment, &status);
    if (!status)
      MeasXoff = (long)round(dblvalue);
    
    sprintf(keyword, "CPHYOFF");
    fits_read_key_dbl(ptr, keyword, &dblvalue, comment, &status);
    if (!status)
      MeasYoff = (long)round(dblvalue);

    if ((MeasXoff != 0) || (MeasYoff != 0)) {
      // Set offset flag
      Offset++;
      // Compute the coordinates of the overlap box between the detection and the measure image
      MeasXmin = MAX(1, (1 + MeasXoff));
      MeasXmax = MIN(Width, (MeasWidth + MeasXoff));
      MeasYmin = MAX(1, (1 + MeasYoff));
      MeasYmax = MIN(Height, (MeasHeight + MeasYoff));
    }
    else {
      MeasXmin = 1;
      MeasXmax = MIN(Width, MeasWidth);
      MeasYmin = 1;
      MeasYmax = MIN(Height, MeasHeight);
    }
    // Compute the size of the overlap box between the detection and the measure image
    OverlapWidth = MeasXmax - MeasXmin + 1;
    OverlapHeight = MeasYmax - MeasYmin + 1;
    break;
    
  case 'r': // Detection image
    // Set image size
    Width = naxes[0];
    Height = naxes[1];
    break;
    
  case 'R': // RMS image
    // Check if RMS image has the same size of the measure image
    if (naxes[0] != MeasWidth) {
      fprintf(stderr, "ERROR: %s has width different from measure image: %d %d\n", filename, naxes[0], MeasWidth);
      return(NULL);
    }
    if (naxes[1] != MeasHeight) {
      fprintf(stderr, "ERROR: %s has height different from measure image: %d %d\n", filename, naxes[0], MeasHeight);
      return(NULL);
    }

    break;
    
  case 's': //Segmentation image
    CheckSize++;
    break;
    
  default:
    break;
  }
  
  if (CheckSize && (naxes[0] != Width)) {
    fprintf(stderr, "ERROR: %s has width different from detection image\n", filename);
    return(NULL);
  }
  if (CheckSize && (naxes[1] != Height)) {
    fprintf(stderr, "ERROR: %s has height different from detection image\n", filename);
    return(NULL);
  }
  
  return(ptr);
}



