#ifndef BOX_H
#define BOX_H

class Box {
  friend class Fits;
  private:
  long lx, ux, ly, uy;
  float cx,cy;
  long npix;
  
 public:
  Box();
  Box(long, long, long, long);
  ~Box();
  long Npix();
  void UpdateCenter();
  void Set(long, long, long, long);
  void Move(long, long);
  long Xdim();
  long Ydim();
  long Lx();
  long Ly();
  float Cx();
  float Cy();
  float Xpos(float);
  float Ypos(float);
  float RadialDistance(float,float);
  bool Contains(float, float);
  bool Contains(Box);
  bool Contains(int, int, int, int);
  bool IsInside(Box);
  bool Overlaps(Box);
  void GrowsToHold(Box);
  void GrowsToHold(int,int,int,int);
  void ShrinksToFit(Box);
  void operator=( Box );
  bool operator==( Box );
};

#endif

