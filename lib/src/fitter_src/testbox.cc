#include <iostream>
#include <iomanip.h>
#include "box.h"

int main(int argc, char *argv[]) {

  Box cell1, cell2,cell12;
  float xx,yy;
  
  //Create a couple of boxes.
  cell1.Set(5,5,10,10);
  cell2.Set(3,3,8,8);
  
  cout << "Cell1 = (5,5) to (10,10) "<<endl;
  cout << "Cell2 = (3,3) to (8,8)" << endl;

  cout << "cell1 is inside cell2 (false): " << cell1.IsInside(cell2) << endl;

  cout << "cell1 overlaps cell2 (true): " << cell1.Overlaps(cell2) << endl;

  cout << "(9,4) is inside cell1: (false) " << cell1.Contains(9,4) << endl;
  cout << "(9,4) is inside cell2: (false) " << cell2.Contains(9,4) << endl;

  cout << "(7,7) is inside cell1: (true)" << cell1.Contains(7,7) << endl;
  cout << "(7,7) is inside cell2: (true)" << cell2.Contains(7,7) << endl;


  cout << "Center position for cell1:" << cell1.Cx() << "," << cell1.Cy() << endl;
  cout << "Center position for cell2:" << cell2.Cx() << "," << cell2.Cy() << endl;
  cout << "Rcell1 distance should be " << sqrt(pow((7-cell1.Cx()),2)+pow((7-cell1.Cy()),2)) << endl;

 cout << "Rcell2 distance should be " << sqrt(pow((7-cell2.Cx()),2)+pow((7-cell2.Cy()),2)) << endl;

  cout << "Rcell1 distance for (7,7) :"<< cell1.RadialDistance(7.,7.) <<endl;
  cout << "Rcell2 distance for (7,7) :"<< cell2.RadialDistance(7.,7.) <<endl;
  cout << endl;

  cout << "GRowing cell 1 to hold cell2" << endl;
  cell1.GrowsToHold(cell2);
  cout << "Center position for cell1:" << cell1.Cx() << "," << cell1.Cy() << endl;
  cout << "Center position for cell2:" << cell2.Cx() << "," << cell2.Cy() << endl;
  cout << "Rcell1 distance should be " << sqrt(pow((7-cell1.Cx()),2)+pow((7-cell1.Cy()),2)) << endl;

 cout << "Rcell2 distance should be " << sqrt(pow((7-cell2.Cx()),2)+pow((7-cell2.Cy()),2)) << endl;

  cout << "Rcell1 distance for (7,7) :"<< cell1.RadialDistance(7.,7.) <<endl;
  cout << "Rcell2 distance for (7,7) :"<< cell2.RadialDistance(7.,7.) <<endl;


  cout << endl;
  cout << "Cell12 = (78,129) to (181,201)"<<endl;
  cell12.Set(78,129,181,201);

  xx=120;
  yy=170;
  cout << "Cell12.cx, cell12.cy = "<<cell12.Cx()<<","<<cell12.Cy()<<endl;
  cout << "Cell 12 contains ("<<xx<<","<<yy<<") (true):"<<cell12.Contains(xx,yy)<<endl;
  cout << "Rcell12 distance is "<< cell12.RadialDistance(xx,yy)<<endl;
  cout << endl;

  xx=159.22;
  yy=199.58;
  cout << "Cell12.cx, cell12.cy = "<<cell12.Cx()<<","<<cell12.Cy()<<endl;
  cout << "Cell 12 contains ("<<xx<<","<<yy<<") (true):"<<cell12.Contains(xx,yy)<<endl;
  cout << "Rcell12 distance is "<< cell12.RadialDistance(xx,yy)<<endl;

  
  cell12.Set(0,0,76,77);
  xx=7.02220011;
  yy=7.72114992;
  cout << "New Rcell12 distance is "<<cell12.RadialDistance(xx,yy)<<endl;
}
