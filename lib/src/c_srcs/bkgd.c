
/*
Copyright © 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdio.h>
#include "fitsio.h"
#include <tphot.h>
#include "globals.h"

/*
  Creates flat background templates.
 */

//#define VERBOSE

double *pixels, *pixels_kern, *mod;
int *pixels_seg;
long fpixel[2] = {1,1};
char * dstdir; 
float CRPIX1=0.0,CRPIX2=0.0;
double CRVAL1=0.e0, CRVAL2=0.e0;
double CD1_1=0.0; double CD2_2=0.0;
char *CTYPE1, *CTYPE2;

extern int ReadCatalog (char * );
extern void WriteThumbnail (char * , char * , int , int , int , double , double * , char * );
extern void WriteHeader(fitsfile *fptr, long naxis1, long naxis2, double totflux, char *kern);
extern FILE *chkfopen(const char * , const char * );
extern fitsfile *chkfitsopen (char * , char );extern long Width, Height;

int main(int argc, char *argv[])
{
fitsfile *fptr1, *fptr2;   /* FITS file pointer, defined in fitsio.h */
  int status = 0;   /* CFITSIO status value MUST be initialized to zero! */
  int bitpix, naxis, ii, i, j, s, l, area, r, shape, idoff;
  long naxes[2] = {1,1};
  int id, xmin, ymin, xmax, ymax;
  float x, y, sss, r2, xc, yc;
  catalogstruct *pCatalog;
  char *logbasename, *TemplDir; 
  char logname[BUFFERSIZE], comments[BUFFERSIZE];
  char *xcorner, *ycorner, *xref, *yref, *cd11, *cd22, *ctyp1, *ctyp2;
  FILE *log;
  char modname[BUFFERSIZE];
  float TOTFLUX=1.0;

  xcorner="CRPIX1"; ycorner="CRPIX2";
  xref="CRVAL1"; yref="CRVAL2";
  cd11="CD1_1"; cd22="CD2_2";
  ctyp1="CTYPE1"; ctyp2="CTYPE2";

  if (argc != 5 || argv[1]=="h") {
    printf("Usage: tphot_make_bkgd_templates TemplDir TemplCat IDoff Shape[c=1/s=0]\n");
    exit(1);
  }

  idoff=atoi(argv[3]);
  shape=atoi(argv[4]);
  TemplDir=argv[1];

  sprintf(logname, "%s/_templates_bkgd.cat", TemplDir);

  if((log = chkfopen(logname, "w")) == NULL)
    return(RETURN_FAILURE);

  /* read templates catalog*/
  if (!ReadCatalog(argv[2]))
    {
      pCatalog = Catalog;
      for (ii = 0; ii < NumSources; ii++) {
#ifdef VERBOSE
	printf("> Creating background template: %7d / %-7d (ID: %d)\n", (ii+1), NumSources, Catalog[ii].id+idoff);
#else
	printf("\33[1M> Creating background template: %7d / %-7d (ID: %d)\n\33[1A", (ii+1), NumSources, Catalog[ii].id+idoff);
#endif	
	id = pCatalog->id;
	r = rand() % 10;
	x = pCatalog->x_image+0.0001*((float)(r)-5.0);
	r = rand() % 10;
	y = pCatalog->y_image+0.0001*((float)(r)-5.0);
	xmin = pCatalog->xmin_image;
	xmax = pCatalog->xmax_image;
	ymin = pCatalog->ymin_image;
	ymax = pCatalog->ymax_image;
	// Write the catalog
	fprintf(log, "%d %f %f %d %d %d %d %e %e\n",
		  id+idoff,x,y,xmin,ymin,xmax,ymax,
		  pCatalog->detbckg,pCatalog->measbckg);
	// Create the thumbnail
	sprintf(modname, "%s/%s-%d.fits", TemplDir, "mod", id);
	status=0;
	if(!fits_open_file(&fptr1, modname, READONLY, &status))
	  {
	    fits_get_img_param(fptr1, 2, &bitpix, &naxis, naxes, &status);
	    fits_read_key(fptr1,TFLOAT,xcorner,&CRPIX1,comments,&status);
	    fits_read_key(fptr1,TFLOAT,ycorner,&CRPIX2,comments,&status);
	    fits_read_key(fptr1,TDOUBLE,xref,&CRVAL1,comments,&status);
	    fits_read_key(fptr1,TDOUBLE,yref,&CRVAL2,comments,&status);
	    fits_read_key(fptr1,TSTRING,ctyp1,&CTYPE1,comments,&status);
	    fits_read_key(fptr1,TSTRING,ctyp2,&CTYPE2,comments,&status);
	    fits_read_key(fptr1,TDOUBLE,cd11,&CD1_1,comments,&status);
	    fits_read_key(fptr1,TDOUBLE,cd22,&CD2_2,comments,&status);
	    fits_close_file(fptr1, &status);
	    xc=((float)naxes[0])/2.0;
	    yc=((float)naxes[1])/2.0;
	    r2=MIN(xc,yc)*MIN(xc,yc);
	    mod=(double *) malloc(naxes[0]*naxes[1] * sizeof(double));
	    sss=0.0;
	    for (i=0;i<naxes[0];i++) 
	      for (j=0;j<naxes[1];j++) 
		{
		  mod[j*naxes[0]+i]=1.0;
		  if (shape){
		    if (((float)i-xc)*((float)i-xc)+((float)j-yc)*((float)j-yc)>r2) mod[j*naxes[0]+i]=0.0;
		  }
		  sss+=mod[j*naxes[0]+i];
		}
	    for (i=0;i<naxes[0]*naxes[1];i++) mod[i]/=sss;
	    WriteThumbnail(TemplDir, "mod", id+idoff, naxes[0], naxes[1], TOTFLUX, mod, "None");
	    free(mod);
	  }
	else
	  {
	    printf("File %s not found\n",modname);
	  }
	pCatalog++;
      }

      fclose(log);
      
    }
}      
