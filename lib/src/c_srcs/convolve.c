#include <string.h>
#include <stdio.h>
#include "fitsio.h"
#include <tphot.h>
#include "globals.h"
#include <fftw3.h>
#include "unistd.h"
#include <nrutil.h>

/*
  Produces smoothed templates from hires FITS cutouts. Writes them.
*/

#define MAX_BCKG_STEP 16
#define DILATION_STEP_FACTOR 1.0
//#define VERBOSE
#define SAVE_TH
//#define SAVE_UNCUT_TH
//#define SAVE_HR_TH
//#define SAVE_MULTIKERNS


static void WriteImage (char *thname, int ThWidth, int ThHeight, 
			double totflux, double *image, char *kern);

double *pixels_kern, *pixels_kern_0, *pixels_th, *pixels, *pixels_temp;; 
int *pixels_seg;
long fpixel[2] = {1,1};
char *dstdir, *indir, *kernfile;
fitsfile *fptr1, *fptr2, *fptr3, *fptr4, *fptr5; 
int rpos,cpos,rneg,cneg,nc,nr, SubBckg; 
long Xoff, Yoff;
double XoffLR, YoffLR;
long xmin,xmax,ymin,ymax;
int count=0;
long LRW=0,LRH=0;
int multi, FFT=1, txt=0, pixratio, norm;
float CRPIX1ref=0.0,CRPIX2ref=0.0;
float CRPIX1=0.0,CRPIX2=0.0;
double CRVAL1=0.e0, CRVAL2=0.e0;
double CD1_1=0.0; double CD2_2=0.0;
char *CTYPE1, *CTYPE2;
double TOL = 1.e-9;
long Width, Height, W_0, H_0, WidthHiRes, HeightHiRes;
float measfl=0.0;

extern float CutOne (int, int, int, int, int, double, double *, int *, int, double *);
static int SmoothOne (catalogstruct *);
static void DoTheConvolutionPix(double *, double *, int, int, double *);
static void DoTheConvolutionFFT(double *, double *, int, int, double *);
static void DoTheConvolutionPixSTRAIGHT(double *, double *, int, int, double *); 
static double* Normalize (double *, int, int);
static void transform(double *, int, int, int, double *);
extern int ReadFilter(char *FilterName);
extern int ReadCatalog (char *CatalogName);
extern void WriteThumbnail (char *dstdir, char *prefix, int id, int ThWidth, 
			    int ThHeight, double totflux, double *image, char *kern);
//extern void WriteHeader(fitsfile *fptr, long naxis1, long naxis2);
extern FILE *chkfopen(const char * filename, const char * input_mode);
extern fitsfile *chkfitsopen (char *filename, char type);

int main(int argc, char *argv[])
{
  int status = 0;   /* CFITSIO status value MUST be initialized to zero! */
  int bitpix, naxis, i, j, ii, jj, s, l, anynul;
  char format[20], hdformat[20], buffer[BUFFERSIZE];
  int NumProcessedSources=0, Kept=0, cut, startcount;
  catalogstruct *pCatalog;
  char *cutcat, *multicat, *shiftsfile;
  char *logbasename; 
  char logname1[BUFFERSIZE],logname2[BUFFERSIZE];
  FILE *log, *shifts; 
  char flag[3], *token;
  char *xcorner, *ycorner, *xref, *yref, *wid, 
    *hei, *cd11, *cd22, *ctyp1, *ctyp2;
  char comments[BUFFERSIZE], range[BUFFERSIZE], newkername[BUFFERSIZE];
  int NumKernels, mkext, nzones, nxzones, nyzones, xzone, yzone;
  int dzonesize, xint, yint, temp, interp, ido;
  double Xx,Yy,absx,absy,x,y,xs,ys,val1,val2,val,sumi;

  if (argc != 21 || argv[1]=="h") {
    printf("Usage: tphot_convolve InDir InCat Kern OutDir OutCat Xoff Yoff LRI 0 None 1 0 HRI SegImage PixRatio SubBckg mkext Shiftfile dzonesize norm\n");
    exit(1);  
  }
  
  // Header keywords
  wid="NAXIS1"; hei="NAXIS2";
  xcorner="CRPIX1"; ycorner="CRPIX2";
  xref="CRVAL1"; yref="CRVAL2";
  cd11="CD1_1"; cd22="CD2_2";
  ctyp1="CTYPE1"; ctyp2="CTYPE2";

  indir=argv[1]; // cutouts directory
  dstdir=argv[4]; // models output directory
  logbasename=argv[5]; // name of output catalog
  Xoff=atoi(argv[6]);  
  Yoff=atoi(argv[7]); 
  NumKernels=atoi(argv[9]);
  multicat=argv[10];
  FFT=atoi(argv[11]);
  txt=atoi(argv[12]);
  pixratio=atoi(argv[15]);
  SubBckg=atoi(argv[16]);
  mkext=atoi(argv[17]);
  norm=atoi(argv[20]);
  shiftsfile=argv[18];

  if (mkext)
    {
      printf("-> Using individual kernels\n");
    }

  XoffLR=(double)Xoff/(double)pixratio;
  YoffLR=(double)Yoff/(double)pixratio;

  // Open and read HRI & Seg
  printf("Reading Images...\n");
  if ((fptr4=chkfitsopen(argv[13],'r'))==NULL) {
    //return(RETURN_FAILURE);
    printf("WARNING: No HiRes image found, will abort if necessary\n");
  }
  else{
  pixels = (double *) malloc(Width*Height * sizeof(double));
  fits_read_pix(fptr4, TDOUBLE, fpixel, Width*Height, NULL,
		pixels, NULL, &status); 
  fits_close_file(fptr4, &status);
  }

  if ((fptr5=chkfitsopen(argv[14],'r'))==NULL) {
    pixels_seg = (int *) malloc(1 * sizeof(int));
    pixels_seg[0]=-1;
    printf("WARNING: No segmentation image found\n");}
  else{
  pixels_seg = (int *) malloc(Width*Height * sizeof(int));
  fits_read_pix(fptr5, TINT, fpixel, Width*Height, NULL,
		pixels_seg, NULL, &status);
  WidthHiRes=Width;
  HeightHiRes=Height;
  fits_close_file(fptr5, &status);}

  /* Open and check LRI to read in some header values */
  if ((fptr3=chkfitsopen(argv[8],'r'))==NULL) return(RETURN_FAILURE);
  fits_read_key(fptr3,TINT,wid,&LRW,comments,&status);
  fits_read_key(fptr3,TINT,hei,&LRH,comments,&status);
  fits_read_key(fptr3,TFLOAT,xcorner,&CRPIX1ref,comments,&status);
  fits_read_key(fptr3,TFLOAT,ycorner,&CRPIX2ref,comments,&status);
  fits_read_key(fptr3,TDOUBLE,xref,&CRVAL1,comments,&status);
  fits_read_key(fptr3,TDOUBLE,yref,&CRVAL2,comments,&status);
  fits_read_key(fptr3,TDOUBLE,cd11,&CD1_1,comments,&status);
  fits_read_key(fptr3,TDOUBLE,cd22,&CD2_2,comments,&status);

  if (NumKernels)
    //If necessary, read in shift values to produce registered kernels
    {
      printf("Shifting kernels using %s\n",shiftsfile);
      shifts = chkfopen(shiftsfile, "r");
    }

  printf("Convolving...\n");

  /* Read hires cutouts catalog */
  cutcat=argv[2];
  if (!ReadCatalog(cutcat))
    {		  
      NumProcessedSources = 0;
      pCatalog = Catalog;

      snprintf(logname1,BUFFERSIZE,"%s",logbasename);
      if((log = chkfopen(logname1, "w")) == NULL)
	return(RETURN_FAILURE);

      fprintf(log, "#id x y xmin ymin xmax ymax cutout_meas_flux SEx_flux_best\n");
      
#ifdef VERBOSE
      printf("Starting convolution with kernel # %d\n",jj);
#endif
      
      kernfile=argv[3];
      /* Open and check kernel images */
      if (txt)
	{
	  // Reads txt file kernel (CONVPHOT)	    
	  // Change the name
	  token = strsep(&kernfile,".");
	  strcat(token, ".txt");
	  kernfile=token;
	  if (ReadFilter(token)) 
	    {
	      printf("Error reading kernel.txt\n");
	      exit(1);
	    }
	}
      else
	{
	  // Reads FITS file kernel
	  if ((fptr2=chkfitsopen(kernfile,'r'))==NULL) return(RETURN_FAILURE);
	  /* Reads kernel matrix.
	     NOTE: Pixel(i,j) is at position i*naxes[0]+j */
	  pixels_kern_0 = (double *) malloc(Width*Height * sizeof(double));
	  fits_read_pix(fptr2, TDOUBLE, fpixel, Width*Height, NULL,
			pixels_kern_0, NULL, &status); 
	  fits_close_file(fptr2, &status);
	  W_0=Width;
	  H_0=Height;
#ifdef VERBOSE
	  printf("Kernel read\n");
#endif
	}
      
      /* Go through the catalog */
      
      //for (ii = 0; ii < NumSources; ii++) {
      ii=0;
      ido=0;
      while(ii<NumSources) {

	// Always initialize copying the generic kernel
	pixels_kern = (double *) malloc(W_0*H_0 * sizeof(double));
	for (i=0;i<W_0*H_0;i++) pixels_kern[i]=pixels_kern_0[i];
	
	if (mkext)
	  {
	    // Check if a kernel for this guy exists
	    sprintf(newkername, "KERNELS/%d_KERNEL.fits", pCatalog->id);
	    //printf("%s %d\n",newkername,pCatalog->id);
	    if( access(newkername, F_OK) != -1) {
	      fptr2=chkfitsopen(newkername,'r');
	      free(pixels_kern);
	      pixels_kern = (double *) malloc(Width*Height * sizeof(double));
	      fits_read_pix(fptr2, TDOUBLE, fpixel, Width*Height, NULL,
			    pixels_kern, NULL, &status); 
	      fits_close_file(fptr2, &status);
	    }
	  }

	if (NumKernels) 
	  // Shift kernel: find the shift,	
	  // move the kernel, and overwrite on the array
	  {
	    xs=0.000; ys=0.000;

	    while (ido<pCatalog->id) {
	      //printf("-> %d %d\n",ido,pCatalog->id);
	      if (fgets(buffer, BUFFERSIZE, shifts) != NULL && !feof(shifts)){
		if ((buffer - '#') == 0 ||  
		    (sscanf(buffer, "%d %lf %lf %lf %lf %d",&ido,&x,&y,&xs,&ys,&interp) !=6)) continue;}}

	    xs=-xs; ys=-ys; // HiRes


	    if (xs!=0.000 || ys!=0.000)
	      {
		
		Xx = xs-(double)((int)xs);
		Yy = ys-(double)((int)ys);
		absx = fabs(Xx);
		absy = fabs(Yy);
		xint=0; yint=0;
		if(absx!=0.0) xint = (int)(Xx/absx);
		if(absy!=0.0) yint = (int)(Yy/absy);
		pixels_temp = (double *) malloc(Width*Height * sizeof(double));
		for (i=0;i<Width*Height;i++) pixels_temp[i]=0.0;
		sumi=0.0;	  
		for (i=MAX(-yint,0);i<Height-MAX(0,yint);i++){
		  for (j=MAX(-xint,0);j<Width-MAX(0,xint);j++){
		    // Bilinear interpolation
		    val1 = (1.0-absx)*pixels_kern[i*Width+j]+absx*pixels_kern[i*Width+(j+xint)];
		    val2 = (1.0-absx)*pixels_kern[(i+yint)*Width+j]+absx*pixels_kern[(i+yint)*Width+(j+xint)];
		    val = (1.0-absy)*val1+absy*val2;
		    temp = (i-(int)ys)*Width+(j-(int)xs);
		    if (temp>=0 && temp<Width*Height) pixels_temp[temp]=val;
		    sumi+=val;
		  }
		}
		for (i=0;i<Width;i++)
		  for (j=0;j<Height;j++)
		    pixels_kern[Width*j+i]=pixels_temp[Width*j+i];
		free(pixels_temp);
#ifdef SAVE_MULTIKERNS
		float TOTFLUX=1.0;
		sprintf(newkername, "kern-%d.fits", pCatalog->id);
		WriteImage(newkername, Width, Height, TOTFLUX, pixels_kern, "None");
#endif
	      }
	  }
	
	if (Width%2==0){
	  cpos=Width/2+1;cneg=cpos-2;}
	else{
	  cpos=(Width+1)/2;cneg=cpos-1;}
	if (Height%2==0){
	  rpos=Height/2+1;rneg=rpos-2;}
	else{
	  rpos=(Height+1)/2;rneg=rpos-1;}

	ii++;
#ifdef VERBOSE
	printf("> Smoothing thumbnail: %7d / %-7d (ID: %d)\n", ii, NumSources, pCatalog->id);
#else
	printf("\33[1M> Smoothing thumbnail: %7d / %-7d (ID: %d)\n\33[1A", ii, NumSources, pCatalog->id);
#endif	
	NumProcessedSources++;
       
	if (!SmoothOne(pCatalog)){
#ifdef VERBOSE
	  printf("Source %d convolved\n",(ii));
#endif
	  Kept++;
	  cut=0;
	  x=pCatalog->x_image; y=pCatalog->y_image;
	  x=x/pixratio-XoffLR; y=y/pixratio-YoffLR;
	  x=MAX(x,(float)xmin); x=MIN(x,(float)xmax);
	  y=MAX(y,(float)ymin); y=MIN(y,(float)ymax);
	  if(xmin==1 || xmax==LRW) cut=cut+1;
	  if(ymin==1 || ymax==LRH) cut=cut+1;
	  fprintf(log, "%d %f %f %ld %ld %ld %ld %e %e\n",
		  pCatalog->id,
		  x,y,
		  xmin,ymin,xmax,ymax,
		  measfl,
		  pCatalog->measbckg);
#ifdef VERBOSE
	  printf("Source %d written on catalog\n",(ii));
#endif
	}
	else{
#ifdef VERBOSE
	  printf("Failed smoothing template %d\n",(ii));
#endif
	}
	if (mkext) free(pixels_kern);
	pCatalog++;
      }
      if (!mkext) free(pixels_kern);
    }
  fclose(log); 
  
  if (pixels!=NULL) free(pixels); 
  free(pixels_seg);
  
  printf("\n");
  printf("Cut thumbnails: %d / %d\n",count,NumProcessedSources);
  printf("Kept thumbnails: %d / %d\n",Kept,NumProcessedSources);  
  
  if (status) fits_report_error(stderr, status); /* print any error message */
  return(status);
}

/* ------------------------------------------------------------------------------ */

int SmoothOne (catalogstruct *Source) {

  /*
    First read in cutout matrix. Then Pad kernel and image. 
    Finally do FFT or straight convolution.
  */

  int status = 0;   /* CFITSIO status value MUST be initialized to zero! */
  int bitpix, naxis, ii, jj, i, j, s, l, cen; 
  long naxes[2] = {1,1};
  char cutname[BUFFERSIZE];
  double bckg, sum, val, cons=1.0, TOTFLUX=0.0;
  double *pixels_kernpad, *pixels_thpad, *mod, *mod0, *mod1, *modl;
  int rows, columns, area, Area, Area1;
  int oi, oj, ai, aj, nc0, nr0, x_in, y_in;
  int flag = 1;
  int he, wi, ar, nrl, ncl, Areal, xof, yof;
  char *totflux,*kernel,comments[BUFFERSIZE];
  int iid, xxmin, yymin, xxmax, yymax, ddx, ddy;
  float xx, yy;
  int border;

  totflux="TOTFLUX";
  kernel="CONVKERN";

  /* If cutout exists, open and read it. If not, read from HiRes image */
  sprintf(cutname, "%s/%s-%d.fits", indir, "cut", Source->id);
  if(!fits_open_file(&fptr1, cutname, READONLY, &status)) 
    /* Open cutout image. Simply use fits_open to avoid repeating long checks */
    {
      fits_get_img_param(fptr1, 2, &bitpix, &naxis, naxes, &status);
      fits_read_key(fptr1,TDOUBLE,totflux,&TOTFLUX,comments,&status);
      columns=naxes[0]; 
      rows=naxes[1];
      area=rows*columns;      
      /* Reads cutout matrix.
	 NOTE: Pixel(i,j) is at position i*naxes[0]+j*/
      pixels_th = (double *) malloc(area * sizeof(double));
      fits_read_pix(fptr1, TDOUBLE, fpixel, area, NULL,
		    pixels_th, NULL, &status); 
      if (status) {
	fits_report_error(stderr, status); /* print any error message */
	printf("Exiting while convolving %s\n",cutname);
	exit(status);
      }
      measfl = Source->detbckg;
    }
  else if (pixels_seg[0]>=0)
    /* Read from HiRes matrix */
    {
      iid = Source->id;
      xx = Source->x_image;
      yy = Source->y_image;
      xxmin = Source->xmin_image;
      xxmax = Source->xmax_image;
      yymin = Source->ymin_image;
      yymax = Source->ymax_image;
      if (xxmax>0 && yymax>0 && xxmin<=WidthHiRes && yymin<=HeightHiRes) {
	if (xxmax-xxmin<3) {
	  ddx = 3-(xxmax-xxmin);
	  xxmin = xxmin - (ddx/2);
	  xxmax = xxmax + (3 - ddx/2);}
	if (yymax-yymin<3) {
	  ddy = 3-(yymax-yymin);
	  yymin = yymin - (ddy/2);
	  yymax = yymax + (3 - ddy/2);}
	xxmin = MAX(1, xxmin);
	xxmax = MIN(xxmax, WidthHiRes);
	yymin = MAX(1, yymin);
	yymax = MIN(yymax, HeightHiRes);
	columns = (xxmax - xxmin + 1);
	rows = (yymax - yymin + 1);
	CRPIX1=CRPIX1ref-(float)xmin+1.; // (float)pCatalog->xmin_image+1;
	CRPIX2=CRPIX2ref-(float)ymin+1.; //(float)pCatalog->ymin_image+1;
	area = rows*columns;
	pixels_th = (double *) malloc(area * sizeof(double));
	for (jj=0; jj<area; jj++) pixels_th[jj]=0.0; 
	bckg = 0.0;
	if (SubBckg) bckg = Source->detbckg;
	measfl = CutOne(iid, xxmin-1, yymin-1, columns, rows, bckg, pixels, pixels_seg, WidthHiRes, pixels_th); // if measfl<0 source is saturated
      }
      else
	{
	  printf("\33[1M>Obj %d out of bounds with xmin %d ymin %d xmax %d ymax %d (%d %d): skipping  \n\33[1A",iid,xxmin,yymin,xxmax,yymax,WidthHiRes,HeightHiRes);
	  return(1);
	}
    }
  else
    {
      printf("WARNING: No cutout, no segmentation found: skipping obj %d\n",Source->id);
    }

  //Now rearrange to convolve. nr and nc are the final dimensions
  
  border=(MAX(Width,Height)+1)/2;
  
  nc=columns+2*border;
  nr=rows+2*border;
  // Make it square and take it to the power of two
  nc = MAX(nc,nr);
  nc = pow(2,ceil(log((double)nc)/log(2))); 
  nr = nc; 
  Area=nc*nr;
  cen=nc/2;
  pixels_thpad = (double *) malloc(Area * sizeof(double));

  /*Now pad cutout*/
  for (ii=0; ii<Area; ii++) {
    pixels_thpad[ii]=0.e0;}
  for (ii=0;ii<rows;ii++){
    for (jj=0;jj<columns;jj++){
      ai=cen-rows/2+ii;aj=cen-columns/2+jj;
      pixels_thpad[ai*nc+aj]=pixels_th[ii*columns+jj];}}

  /*//This would print out the matrix
    printf("---------\n");
    for (ii=0;ii<nc;ii++)		
    {for (jj=0; jj<nr; jj++)
    {s=ii*nr+jj;
    printf("%f\t",pixels_thpad[s]);}
    printf("\n");}*/
  
  mod0 = (double *) malloc(Area * sizeof(double));
  for (ii=0;ii<Area;ii++) mod0[ii]=0.0;

  /* Now pad+wrap kernel, and "do the convolution, baby" */
  if (FFT) {  
    
    pixels_kernpad = (double *) malloc(Area * sizeof(double));

    for (ii=0; ii<Area; ii++) {
      pixels_kernpad[ii]=0.e0;}
    for (ii=0; ii<rpos; ii++){
      for (jj=0; jj<cpos; jj++){
	oi=rneg+ii;oj=cneg+jj;
	pixels_kernpad[ii*nc+jj]=pixels_kern[oi*Width+oj];}}
    for (ii=0; ii<rneg; ii++){
      for (jj=0; jj<cpos; jj++){
	ai=nr-rneg+ii;oj=cneg+jj;
	pixels_kernpad[ai*nc+jj]=pixels_kern[ii*Width+oj];}}
    for (ii=0; ii<rpos; ii++){
      for (jj=0; jj<cneg; jj++){
	oi=rneg+ii;aj=nc-cneg+jj;
	pixels_kernpad[ii*nc+aj]=pixels_kern[oi*Width+jj];}}
    for (ii=0; ii<rneg; ii++){
      for (jj=0; jj<cneg; jj++){
	ai=nr-rneg+ii;aj=nc-cneg+jj;
	pixels_kernpad[ai*nc+aj]=pixels_kern[ii*Width+jj];}}
    
    // FFT convolution: this is correctly wrapped/flipped!
    // Checked with on-line examples
    DoTheConvolutionFFT(pixels_kernpad, pixels_thpad, nr, nc, mod0);
  }
  else
    {
      // For straight pixel convolution flip the kernel.
      pixels_kernpad = (double *) malloc(Width*Height * sizeof(double));
      for (ii=Width*Height-1;ii>=0;ii--)
	{
	  jj=Width*Height-ii-1;
	  pixels_kernpad[jj]=pixels_kern[ii];
	}
      
      DoTheConvolutionPix(pixels_kernpad, pixels_thpad, nc, nr, mod0);
    }
#ifdef VERBOSE
  printf("Template convolved\n");
#endif
  
  /*//This would print out the matrix
    printf("----------------\n");
    for (ii=0;ii<nr;ii++)		
    {for (jj=0; jj<nc; jj++)
    {s=ii*nc+jj;
    printf("%f\t",mod0[s]);}
    printf("\n");}*/

  // Now cut the convolved matrix to the right dimension;
  // it is Ncutout+Nkernel
  //wi = MIN(Width+columns-1,nc)+2;
  //he = MIN(Height+rows-1,nr)+2;
  wi = MIN(Width+columns,nc);
  he = MIN(Height+rows,nr);
  ar = wi*he;
  mod = (double *) malloc(ar * sizeof(double));
  aj = cen-wi/2;
  ai = cen-he/2;
  
  for(ii=0;ii<he;ii++){
    for(jj=0;jj<wi;jj++){
      mod[ii*wi+jj]=mod0[(ai+ii)*nc+(aj+jj)];
    }
  }
  free(mod0); mod0=NULL;
  Area=ar;
  nc=wi;
  nr=he;
  
  /*//This would print out the matrix
    printf("----------------\n");
    for (ii=0;ii<nr;ii++)		
    {for (jj=0; jj<nc; jj++)
    {s=ii*nc+jj;
    printf("%f\t",mod[s]);}
    printf("\n");}*/
  
#ifdef SAVE_HR_TH
  WriteThumbnail(dstdir, "HR_mod", Source->id, nc, nr, TOTFLUX, mod, kernfile);
#endif
  
  int xd,yd,xu,yu;
  xmin = Source->xmin_image-(nc/2-columns/2)-Xoff;
  ymin = Source->ymin_image-(nr/2-rows/2)-Yoff;
  xmax = xmin+nc-1;
  ymax = ymin+nr-1;
  xd=xmin; yd=ymin; xu=xmax; yu=ymax;

  // Find the region on which averages will be computed
  while((xmin-1)%pixratio) xmin-=1;
  while((ymin-1)%pixratio) ymin-=1;
  while(xmax%pixratio) xmax+=1;
  while(ymax%pixratio) ymax+=1;
  ncl=xmax-xmin+1;
  nrl=ymax-ymin+1;

  /* If pixratio>1, bin down the template */
  if (pixratio>1){
    
    mod1 = (double *) malloc(nc*nr * sizeof(double));
    for (ii=0;ii<nc*nr;ii++) mod1[ii]=mod[ii];
    free(mod); mod=NULL;

    Areal=ncl*nrl;
    modl = (double *) malloc(Areal * sizeof(double));
    for (ii=0;ii<Areal;ii++) modl[ii]=0.0;

    xof=xd-xmin;  //(ncl-nc)/2;
    yof=yd-ymin;  //(nrl-nr)/2;

    for (i=0;i<nc;i++){
      for (j=0;j<nr;j++){
	modl[ncl*(j+yof)+(i+xof)]=mod1[nc*j+i];
      }
    }
    free(mod1); mod1=NULL;

    // Scale down to lores pixel scale
    if (ncl%pixratio) {
      printf("TERROR! %d %d %d\n",Source->id,ncl,pixratio); exit(1);}
    if (nrl%pixratio) {
      printf("TERROR! %d %d %d\n",Source->id,nrl,pixratio); exit(1);}
    nc=ncl/pixratio;
    nr=nrl/pixratio;
    Area=nc*nr;
    mod = (double *) malloc(Area * sizeof(double));
    for (i=0; i<Area; i++) mod[i]=0.0;

    x_in=0; 
    for (ii=0;ii<nc;ii++){
      
      y_in=0;
      for (jj=0;jj<nr;jj++){
	
	val=0.0;
	for (i=x_in;i<x_in+pixratio;i++){
	  for (j=y_in;j<y_in+pixratio;j++){
	    val+=modl[ncl*j+i]/cons;
	  }
	}
	
	mod[nc*jj+ii]=val;
	
	y_in+=pixratio;
      }
      x_in+=pixratio;
    }
    
    xmin=(int)ceil((double)xmin/(double)pixratio);
    ymin=(int)ceil((double)ymin/(double)pixratio);
    xmax=xmin+nc-1;
    ymax=ymin+nr-1;

    free(modl); modl=NULL;
   
#ifdef VERBOSE
    printf("Template rebinned\n");
#endif
  }
  
  /*If the models overlaps LRI limits, cut it */
  if (xmax>LRW || xmin<1 || ymax>LRH || ymin<1)
    {
      
      // Check if it is completely outside LRI
     
      if (xmax<1 || xmin>LRW || ymax<1 || ymin>LRH)
	{
#ifdef VERBOSE
	  printf("Model %d is outside LRI frame!\n", Source->id);
#ifdef SAVE_UNCUT_TH
	  CRPIX1=CRPIX1ref-xmin+1;
	  CRPIX2=CRPIX2ref-ymin+1;
	  if (norm==1) mod=Normalize(mod,Area,Source->id);
	  if (mod[0]>-1.e11) { 
	    WriteThumbnail(dstdir, "culled_mod", Source->id, nc, nr, TOTFLUX, mod, kernfile);}
#endif
#endif
	}
      else
	{
	  xmin = MAX( xmin , 1 );
	  xmax = MIN( xmax , LRW );
	  ymin = MAX( ymin , 1 );
	  ymax = MIN( ymax , LRH );
	  
	  CRPIX1=CRPIX1ref-xmin+1;
	  CRPIX2=CRPIX2ref-ymin+1;
	  count=count+1;
	  nc0=nc;nr0=nr;
	  x_in=0;y_in=0;
	  if(xmin==1) x_in=nc-(xmax-xmin+1);
	  if(ymin==1) y_in=nr-(ymax-ymin+1);
	  nc=xmax-xmin+1;
	  nr=ymax-ymin+1;
	  Area1=nc*nr;
	  
	  mod1 = (double *) malloc(Area1 * sizeof(double));
	  for (ii=0;ii<Area1;ii++) mod1[ii]=0.0;

	  for (ii=0;ii<nr;ii++){
	    for (jj=0;jj<nc;jj++){
	      mod1[ii*nc+jj]=mod[(y_in+ii)*nc0+(x_in+jj)];}}	
#ifdef SAVE_UNCUT_TH
	  if (norm==1) mod=Normalize(mod,Area,Source->id);
	  if (mod[0]>-1.e11) { 
	    WriteThumbnail(dstdir, "uncut_mod", Source->id, nc0, nr0, TOTFLUX, mod, kernfile);}
#endif
      
	  if (norm==1) mod1=Normalize(mod1,Area1,Source->id);
	  if (mod1[0]>-1.e11) { 
	    flag=0;
#ifdef SAVE_TH
	    WriteThumbnail(dstdir, "mod", Source->id, nc, nr, TOTFLUX, mod1, kernfile);
#endif
	  }
	  free(mod1);
	  mod1=NULL;
	  
	}
    }
  else
    {

      if (nc!=xmax-xmin+1 || nr!=ymax-ymin+1) {
	printf(">>> %d: %ld %ld %ld %ld %ld %ld %ld %ld %d %d\n",Source->id,xmin,xmax,ymin,ymax,Xoff,Yoff,LRW,LRH,nc,nr);
	exit(1);
      }
      CRPIX1=CRPIX1ref-xmin+1;
      CRPIX2=CRPIX2ref-ymin+1;
      if (norm==1) mod=Normalize(mod,Area,Source->id);
      if (mod[0]>-1.e11) { 
	flag=0;
#ifdef SAVE_TH
	WriteThumbnail(dstdir, "mod", Source->id, nc, nr, TOTFLUX, mod, kernfile); 
#endif
      }
    }
  
  /* //This would print out the matrix
     printf("mod---------\n");
     for (ii=0;ii<nc;ii++)		
     {for (jj=0; jj<nr; jj++)
     {s=ii*nr+jj;
     printf("%f\t",mod[s]);}
     printf("\n");} */
  
  free(mod);
  mod=NULL;
  
  free(pixels_th); free(pixels_thpad); free(pixels_kernpad);
  
  pixels_th=NULL; pixels_thpad=NULL; pixels_kernpad=NULL;
  fits_close_file(fptr1, &status);

#ifdef VERBOSE
  printf("Template cut and saved\n");
#endif
  return(flag);
}

/* ------------------------------------------------------------------------------ */

void DoTheConvolutionFFT (double * in1, double * in2, int nr, int nc, double * conv)
{
  int i;
  int nch;
  fftw_complex *out1,*out2,*out;
  fftw_plan plan_backward;
  fftw_plan plan_forward1, plan_forward2;

  nch = ( nc / 2 ) + 1;
  out = fftw_malloc ( sizeof ( fftw_complex ) * nr*nch );
  out1 = fftw_malloc ( sizeof ( fftw_complex ) * nr*nch );
  out2 = fftw_malloc ( sizeof ( fftw_complex ) * nr*nch );
  plan_forward1 = fftw_plan_dft_r2c_2d ( nr,nc, in1, out1, FFTW_ESTIMATE );
  plan_forward2 = fftw_plan_dft_r2c_2d ( nr,nc, in2, out2, FFTW_ESTIMATE );
  fftw_execute ( plan_forward1 ); fftw_execute ( plan_forward2 );
  
  for (i=0;i<nr*nch;i++){
    out[i][0]=out1[i][0]*out2[i][0]-out1[i][1]*out2[i][1];
    out[i][1]=out1[i][0]*out2[i][1]+out1[i][1]*out2[i][0];
  }

  plan_backward = fftw_plan_dft_c2r_2d ( nr,nc, out, conv, FFTW_ESTIMATE );
  fftw_execute ( plan_backward );

  for ( i = 0; i < nr*nc; i++ ){conv[i] = conv[i] / ( double ) ( nr*nc ) ;}
  fftw_destroy_plan ( plan_forward1 );fftw_destroy_plan ( plan_forward2 );
  fftw_destroy_plan ( plan_backward );
  fftw_free ( out1 );fftw_free ( out2 ); fftw_free( out );

}


/* ------------------------------------------------------------------------------ */

void DoTheConvolutionPix (double *ker, double *field, int sw, int sh, double *scan) {

  // Does the convolution CONVPHOT-like
  int mw, mh, mw2, mh2, m0, me, m, mx, dmx, y0, dy;
  int fy, fymin, fymax;
  int i, j, Area;
  double *mask, *mscan;
  double *mscane, *s, *s0, *d, *de, mval; 

  Area=nc*nr;
  fymin = 0;
  fymax = sh;

  for(i = 0; i < sh; i++) {
    fy = i;

    mscan = scan + (i * sw);

    mw = Width;
    mh = Height;
    mw2 = mw/2;
    mh2 = mh/2;
    mscane = mscan + sw;
    y0 = fy - mh2;
    
    dy = fymin - y0;
    
    if (dy > 0) {
      m0 = mw * dy;
      y0 = fymin;
    }
    else
      m0 = 0;
    
    dy = fymax - y0;
    if (dy < mh)
      me = mw * dy;
    else
      me = mw * mh;
    
    memset(mscan, 0, sw * sizeof(double));
    mask = ker + m0;

    for (m = m0, mx = 0; m < me; m++, mx++) {
      if (mx == mw)
	mx = 0;
      if (!mx) {
	s0 = field + sw * y0;
	y0++;
      }
      
      dmx = mx - mw2;
      
      if (dmx >= 0) {
	s = s0 + dmx;
	d = mscan;
	de = mscane - dmx;
      }
      else {
	s = s0;
	d = mscan - dmx;
	de = mscane;
      }
      
      mval = *(mask++); 
      while (d < de) 
	*(d++) += mval * *(s++);
    }
  }

}

/* ------------------------------------------------------------------------------ */

double* Normalize (double *in, int dim, int idu)
/* Masks out bad pixels and normalize to 1 */
{
  int ii;
  double sum=0.e0;

  for (ii=0;ii<dim;ii++){
    //if ((in[ii])<TOL) in[ii]=0.e0; // masking here?
    sum+=in[ii];
  }
  if (sum>TOL) 
    {
      for (ii=0;ii<dim;ii++){
	in[ii]=in[ii]/(sum);
	//if ((in[ii])<TOL) in[ii]=0.e0;
      }
    }
  else
    {
      in[0]=-1.e12;
#ifdef VERBOSE
      printf("Bad normalization! ID= %d SUM= %f\n",idu, sum);
#endif
    }
  return(in);
}

/* -------------------------------------------------------------------------- */


void DoTheConvolutionPixSTRAIGHT(double *ker, double *field, int nc, int nr, double * scan) {

  // Direct, straight, SLOW implementation on convolution
  int mw, mh, mw2, mh2;
  int i, j, ii, jj, iii, jjj, Area;

  Area=nc*nr;
  scan = (double *) malloc(Area * sizeof(double));
  for(i = 0; i < Area; i++) scan[i]=0.0;

  for (i=0;i<nr;++i){
    for (j=0;j<nc;++j){

      for (mh=0;mh<Height;++mh){

	ii=Height-1-mh;
	for (mw=0;mw<Width;++mw){
	    
	  jj=Width-1-mw;

	  iii=i+(mh-rpos);
	  jjj=j+(mw-cpos);

	  if(iii>=0 && iii<nr && jjj>=0 && jjj<nc)
	        
	    scan[nc*i+j]+=field[nc*iii+jjj]*ker[Width*mh+mw];
	    
	}
      }
    }
  }

}















void WriteImage (char *thname, int ThWidth, int ThHeight, double totflux, double *image, char *kern) {

  void TestFitsExistence(char *FitsName);

  fitsfile *thptr;
  //char thname[BUFFERSIZE];
  int status = 0; /*  CFITSIO status value MUST be initialized to zero! */
  int i;
  long firstpix[2] = {1,1};
  double *Row, *ptr;
  void WriteHeader(fitsfile *fptr, long naxis1, long naxis2, double totflux, char *kern);

  /* Allocate memory for storing double image row */
  QMALLOC(Row, double, ThWidth);
  
  //sprintf(thname, "%s/%s%s.fits", dstdir, prefix, id);
#ifdef INTERACTIVE
  TestFitsExistence(thname);
#endif
  remove(thname);
  if (fits_create_file(&thptr, thname, &status)) {
    fits_report_error(stderr, status);
    exit(EXIT_FAILURE);
  }
  
  WriteHeader(thptr, ThWidth, ThHeight, totflux, kern);
  
  ptr = image;
  for (firstpix[1] = 1; firstpix[1] <= ThHeight; firstpix[1]++) {
    for (i = 0; i < ThWidth; i++) {
      Row[i] = *ptr;
      ++ptr;
    }
    if(fits_write_pix(thptr, TDOUBLE, firstpix, ThWidth, Row, &status)) {
      fits_report_error(stderr, status);
      exit(EXIT_FAILURE);
    }          
  }
  
  QFREE(Row);
  
  if(fits_close_file(thptr, &status)) {
    fits_report_error(stderr, status);
    exit(EXIT_FAILURE);
  }    
}

