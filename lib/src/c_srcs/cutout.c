
/*
Copyright © 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdio.h>
#include "fitsio.h"
#include <tphot.h>
#include "globals.h"

/*
  Produces cutouts from hires image. Writes them if desired.
  If desired also subtracts background and does some more checkings.
*/

#define MAX_BCKG_STEP 16
#define DILATION_STEP_FACTOR 1.0
//#define VERBOSE
#define SAVE_TH
//#define COMP_BCKG

double *pixels, *pixels_kern, *cut;
int *pixels_seg;
long fpixel[2] = {1,1};
char * dstdir; 
float CRPIX1=0.0,CRPIX2=0.0;
double CRVAL1=0.e0, CRVAL2=0.e0;
double CD1_1=0.0; double CD2_2=0.0;
char *CTYPE1, *CTYPE2;

extern float CutOne (int, int, int, int, int, double, double *, int *, int, double *);
extern int ReadCatalog (char * );
extern void WriteThumbnail (char * , char * , int , int , int , double , double * , char * );
extern void WriteHeader(fitsfile * , long , long , double);
extern FILE *chkfopen(const char * , const char * );
extern fitsfile *chkfitsopen (char * , char );
extern long Width, Height;

int main(int argc, char *argv[])
{
  fitsfile *fptr1, *fptr2;   /* FITS file pointer, defined in fitsio.h */
  int status = 0;   /* CFITSIO status value MUST be initialized to zero! */
  int bitpix, naxis, ii, jj, s, l, area, SubBckg;
  int id, xmin, ymin, xmax, ymax, dx, dy, columns, rows;
  float x, y, measfl;
  int NumProcessedSources=0;
  catalogstruct *pCatalog;
  char *logbasename; 
  char logname[BUFFERSIZE], comments[BUFFERSIZE];
  char *xcorner, *ycorner, *xref, *yref, *cd11, *cd22, *ctyp1, *ctyp2;
  FILE *log;
  float CRPIX1ref=0.0,CRPIX2ref=0.0;
  double *cut;
  double bckg;
  int saturated=0;

  if (argc != 7 || argv[1]=="h") {
    printf("Usage: tphot_cutout HRI SegImage Cat OutDir OutCat SubBckg\n");
    exit(1);
  }

  logbasename=argv[5];
#ifdef SAVE_TH
  dstdir=argv[4];
#endif
  SubBckg=atoi(argv[6]);

  xcorner="CRPIX1"; ycorner="CRPIX2";
  xref="CRVAL1"; yref="CRVAL2";
  cd11="CD1_1"; cd22="CD2_2";
  ctyp1="CTYPE1"; ctyp2="CTYPE2";  

  /* Open and check detection image*/
  printf("Reading images...\n");
  if ((fptr1=chkfitsopen(argv[1],'r'))==NULL) return(RETURN_FAILURE);
  fits_read_key(fptr1,TFLOAT,xcorner,&CRPIX1ref,comments,&status);
  fits_read_key(fptr1,TFLOAT,ycorner,&CRPIX2ref,comments,&status);
  fits_read_key(fptr1,TDOUBLE,xref,&CRVAL1,comments,&status);
  fits_read_key(fptr1,TDOUBLE,yref,&CRVAL2,comments,&status);
  fits_read_key(fptr1,TSTRING,ctyp1,&CTYPE1,comments,&status);
  fits_read_key(fptr1,TSTRING,ctyp2,&CTYPE2,comments,&status);
  fits_read_key(fptr1,TDOUBLE,cd11,&CD1_1,comments,&status);
  fits_read_key(fptr1,TDOUBLE,cd22,&CD2_2,comments,&status);

  /* Open and check segmentation image*/
  if ((fptr2=chkfitsopen(argv[2],'s'))==NULL) return(RETURN_FAILURE);

  /* read hires sextractor catalog; "Catalog" has values*/
  if (!ReadCatalog(argv[3]))
    {		  
      /* Now read in values from images.
         Pixels is the matrix with scientific value.
         Pixels_seg, well, you get it
         NOTE: Pixel(i,j) is at position i*naxes[0]+j */
      // Detection image
      pixels = (double *) malloc(Width*Height * sizeof(double));
      fits_read_pix(fptr1, TDOUBLE, fpixel, Width*Height, NULL,
		    pixels, NULL, &status); 
      // Segmentation image
      pixels_seg = (int *) malloc(Width*Height * sizeof(int));
      fits_read_pix(fptr2, TINT, fpixel, Width*Height, NULL,
		    pixels_seg, NULL, &status);
      
      /* //This would print out the matrix, in "correct" (not FITS) shape.
	 for (ii=0;ii<Height;ii++)		
	 {for (jj=0; jj<Width; jj++)
	 {s=ii*Width+jj;
	 printf("%f\t",pixels[s]);}
	 printf("\n");} */

      pCatalog = Catalog;

      printf("Cutting...\n");
      if((log = chkfopen(logbasename, "w")) == NULL)
	return(RETURN_FAILURE);
      fprintf(log, "#id x y xmin ymin xmax ymax cutout_meas_flux SEx_flux_best\n"); 

      /*Go through the catalog and make cutouts.*/
      NumProcessedSources = 0;
      for (ii = 0; ii < NumSources; ii++) {
#ifdef VERBOSE
	printf("> Storing thumbnail: %7d / %-7d (ID: %d)\n", (ii+1), NumSources, Catalog[ii].id);
#else
	printf("\33[1M> Storing thumbnail: %7d / %-7d (ID: %d)\n\33[1A", (ii+1), NumSources, Catalog[ii].id);
#endif
	//FilterHalfWidth=0; FilterHalfHeight=0; // TEMPORARYLY COMMENTED
	id = pCatalog->id;
	x = pCatalog->x_image;
	y = pCatalog->y_image;
	xmin = pCatalog->xmin_image;
	xmax = pCatalog->xmax_image;
	ymin = pCatalog->ymin_image;
	ymax = pCatalog->ymax_image;
	if (xmax>0 && ymax>0 && xmin<=Width && ymin<=Height) {
	  if (xmax-xmin<3) {
	    dx = 3-(xmax-xmin);
	    xmin = xmin - (dx/2);  
	    xmax = xmax + (3 - dx/2);}
	  if (ymax-ymin<3) {
	    dy = 3-(ymax-ymin);
	    ymin = ymin - (dy/2);
	    ymax = ymax + (3 - dy/2);}
	  xmin = MAX(1, xmin);
	  xmax = MIN(xmax, Width);
	  ymin = MAX(1, ymin);
	  ymax = MIN(ymax, Height);
	  columns = (xmax - xmin + 1);
	  rows = (ymax - ymin + 1);
	  CRPIX1=CRPIX1ref-(float)xmin+1.; // (float)pCatalog->xmin_image+1;
	  CRPIX2=CRPIX2ref-(float)ymin+1.; //(float)pCatalog->ymin_image+1;	
	  area = rows*columns;
	  cut = (double *) malloc(area * sizeof(double));
	  for (jj=0; jj<area; jj++) cut[jj]=0.0; 
	  bckg = 0.0;
	  if (SubBckg) bckg = pCatalog->detbckg;
	  measfl = CutOne(id, xmin-1, ymin-1, columns, rows, bckg, pixels, pixels_seg, Width, cut); // if measfl<0 source is saturated
	  if (measfl<0.) saturated+=1;
	  WriteThumbnail(dstdir, "cut", id, columns, rows, 1.0, cut, "None");
	  free(cut);
	  
	  // Print out cutout catalog. 
	  fprintf(log, "%d %f %f %d %d %d %d %e %e\n",
		  id,x,y,xmin,ymin,xmax,ymax,
		  measfl,pCatalog->measbckg);
	  NumProcessedSources++;
	}
	else
	  {
	    printf("Obj %d out of bounds with xmin %d ymin %d xmax %d ymax %d (%d %d): skipping  \n",id,xmin,ymin,xmax,ymax,Width,Height);
	  }
	pCatalog++;		
      }	      
    }
  printf("\nKept sources: %d \n",NumProcessedSources);
  printf("Found %d saturated sources! \n",saturated);
  fclose(log);
  fits_close_file(fptr1, &status);
  fits_close_file(fptr2, &status);
  free(pixels); 
  free(pixels_seg);
  if (status) fits_report_error(stderr, status); /* print any error message */
  return(status);
}

