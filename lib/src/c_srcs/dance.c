#include <string.h>
#include <stdio.h>
#include "fitsio.h"
#include <tphot.h>
#include "globals.h"
#include <fftw3.h>
#include <nrutil.h>

/* 
   Individual registration of kernels
*/ 

//#define VERBOSE
#define BIG_REAL 100000.0

double *pixels, *pixels_col, *pixels_kern;
double **corr, *xmarg, *ymarg, *_dx, *_dy, *x, *y, *corrweight;
double TOL = 1.e-9, TOTFLUX = 1.0;
double threshold;
float sumix, sumi, xsh, ysh, ddx, ddy, mean, max_shift, val, valmax;
long fpixel[2] = {1,1}, LRW=0,LRH=0;
long Width, Height, nx, ny, xini, xend, yini, yend;
int xlag,ylag,temp, xzone, yzone, xsize, ysize, xsize0, ysize0;
int NX, NY, NXp2, NYp2, Xoff, Yoff, txt, FFT, NumProcessedSources;
int *interp, *ids, i, j, ii, jj, obj, xwindow, ywindow;
int xshift, yshift, imax, jmax, npix;
int xcbox=5, ycbox=5, xlo, xhi, ylo, yhi;
char *dstdir, *prefix, id[9];
fitsfile *fptr1, *fptr2, *fptr3;
float absx, absy, val1, val2, Rneigh;
float CRPIX1ref=0.0,CRPIX2ref=0.0;
float CRPIX1=0.0,CRPIX2=0.0;
double CRVAL1=0.e0, CRVAL2=0.e0;
double CD1_1=0.0; double CD2_2=0.0;
char *CTYPE1, *CTYPE2;


static float CorrFunc();
static float CorrFuncFFT();
static double* DoTheConvolutionFFT (double *in1, double *in2, int nr, int nc);
static void FindShifts();
static void Interpol(long, float, float);
static void Interpol_Rfix(long, float);
extern int ReadCatalog (char *Catalogname);
extern fitsfile *chkfitsopen (char *filename, char type);
extern FILE *chkfopen(const char * filename, const char * input_mode);

int main(int argc, char *argv[])
{

  int status = 0; /* CFITSIO status value MUST be initialized to zero! */
  int bitpix, naxis, xint, yint, pixratio, neighc;
  float absx, absy, val1, val2, bkg;
  double XoffLR, YoffLR;
  catalogstruct *pCatalog;
  char *xcorner, *ycorner, *xref, *yref, *wid, *hei, *cd11, *cd22;
  char comments[BUFFERSIZE], *kernelname;
  char *logbasename, *logbasename1, *incat;
  char kername[BUFFERSIZE];
  char logname[BUFFERSIZE], logname1[BUFFERSIZE], lognamek[BUFFERSIZE];
  char newkername[BUFFERSIZE], range[BUFFERSIZE];
  char buffer[BUFFERSIZE], mckn[BUFFERSIZE], mccn[BUFFERSIZE];
  FILE *log, *log1, *logk; 
  FILE *multicutnew, *multikernew, *multikern;

  if (argc != 21 || argv[1]=="h") {
    printf("Usage: tphot_dance LRI Collage xshift[5] yshift[5] CellXdim CellYdim maxshift[1.0] DcardFile LogFile KernFile PrefixKern OutDir Xoff Yoff PixRatio KernTxt[0] FFT[0] InputTemplCat N_Neigh_Interp BkgdPix\n");
    exit(1);  
  }

  xshift=atoi(argv[3]);
  yshift=atoi(argv[4]);
  xsize0=atoi(argv[5]);
  ysize0=atoi(argv[6]);
  max_shift=atof(argv[7]);
  xshift=MAX(xshift,(int)max_shift);
  yshift=MAX(yshift,(int)max_shift);
  logbasename=argv[8];
  logbasename1=argv[9];
  kernelname=argv[10];
  prefix=argv[11];
  dstdir=argv[12];
  Xoff=atoi(argv[13]);
  Yoff=atoi(argv[14]);
  pixratio=atoi(argv[15]);
  txt=atoi(argv[16]);
  FFT=atoi(argv[17]);
  incat=argv[18];
  neighc=atoi(argv[19]);
  bkg=atof(argv[20]);

  /* Open and check LRI */
  if ((fptr1=chkfitsopen(argv[1],'r'))==NULL) return(RETURN_FAILURE);
  pixels = (double *) malloc(Width*Height * sizeof(double));
  fits_read_pix(fptr1, TDOUBLE, fpixel, Width*Height, NULL,
		pixels, NULL, &status);
  LRW=Width; LRH=Height;
  for (i=0;i<LRW*LRH;i++) pixels[i]-=bkg; // Background
  sumi=0.0;
  for (i=0;i<LRW*LRH;i++){sumi+=pixels[i];}
  for (i=0;i<LRW*LRH;i++){pixels[i]=pixels[i]/sumi;}

  /* Open and check collage image */
  if ((fptr2=chkfitsopen(argv[2],'r'))==NULL) return(RETURN_FAILURE);
  pixels_col = (double *) malloc(Width*Height * sizeof(double));
  fits_read_pix(fptr2, TDOUBLE, fpixel, Width*Height, NULL,
		pixels_col, NULL, &status);
  if (Width!=LRW || Height!=LRH){
    printf("TERROR! Collage and LRI have different sizes. Aborting\n");
    exit(1);}
  sumi=0.0;
  
  /*for (i=0;i<LRW*LRH;i++){sumi+=pixels_col[i];}
    for (i=0;i<LRW*LRH;i++){pixels_col[i]=pixels_col[i]/sumi;}*/

  xwindow=2*xshift+1;
  ywindow=2*yshift+1;

  // Initialize output files
  snprintf(logname,BUFFERSIZE,"%s",logbasename);
  if((log = chkfopen(logname, "w")) == NULL)
    return(RETURN_FAILURE);
  snprintf(logname1,BUFFERSIZE,"%s",logbasename1);
  if((log1 = chkfopen(logname1, "w")) == NULL)
    return(RETURN_FAILURE);
  fprintf(log1, "#  1    id\n#  2    cx\n#  3    cy\n#  4    xshift\n#  5    yshift\n#  6    interpolated\n");

  /* -------------- Begin loop on objects ------------- */

  if (!ReadCatalog(incat)){

    _dx = dvector(1, (long)NumSources);
    _dy = dvector(1, (long)NumSources);
    corrweight = dvector(1, (long)NumSources);
    ids = ivector(1, (long)NumSources);
    x = dvector(1, (long)NumSources);
    y = dvector(1, (long)NumSources);
    interp = ivector(1, (long)NumSources);

    Rneigh=3.0*sqrt((float)(LRW*LRH)/(float)NumSources);

    NumProcessedSources = 0;
    pCatalog = Catalog;

    for (obj = 1; obj <= NumSources; obj++) {
      printf("\33[1M> Processing %7d / %-7d (ID: %d)\n\33[1A", obj, NumSources, pCatalog->id);

      _dx[obj]=1000.0;
      _dy[obj]=1000.0;
      corrweight[obj]=0.0;

      ids[obj] = pCatalog->id;
      x[obj] = pCatalog->x_image;
      y[obj] = pCatalog->y_image;
      xini = pCatalog->xmin_image;
      xend = pCatalog->xmax_image;
      yini = pCatalog->ymin_image;
      yend = pCatalog->ymax_image;
      if (xend-xini<xsize0) {
	ddx = xsize0-(xend-xini);
	xini = xini - (ddx/2);  
	xend = xend + (xsize0 - ddx/2);}
      if (yend-yini<ysize0) {
	ddy = ysize0-(yend-yini);
	yini = yini - (ddy/2);
	yend = yend + (ysize0 - ddy/2);}
      xini = MAX(1, xini);
      xend = MIN(xend, Width);
      yini = MAX(1, yini);
      yend = MIN(yend, Height);
      xsize=xend-xini+1;
      ysize=yend-yini+1;

      /* Compute correlation function */
      corr = dmatrix(1, (long)xwindow, 1, (long)ywindow);
      if (FFT) 
	{
	  corrweight[obj]=CorrFuncFFT();
	}
      else
	{
	  corrweight[obj]=CorrFunc();
	}

      // Define the centering box
      xlo = MAX(1,imax-xcbox/2);
      xhi = MIN(xwindow,imax+xcbox/2);
      nx  = xhi-xlo+1;
      ylo = MAX(1,jmax-ycbox/2);
      yhi = MIN(ywindow,jmax+ycbox/2);  
      ny  = yhi-ylo+1;
#ifdef VERBOSE
      printf("... %d %d %d %d %ld %d %d %ld\n",imax,jmax,xlo,xhi,nx,ylo,yhi,ny);
#endif
  
      xmarg = dvector(1,nx);
      ymarg = dvector(1,ny);

      /* Find the peak of the correlation function */
      FindShifts();

      // Only keep non-spurious shifts
      if (fabs(xsh)<=max_shift && fabs(ysh)<=max_shift){
	_dx[obj] = xsh;
	_dy[obj] = ysh;
      }

      free(ymarg);
      free(xmarg);
      free(corr);

      NumProcessedSources++;
      pCatalog++;
    
    }

    fits_close_file(fptr1, &status); 
    fits_close_file(fptr2, &status);
    
    /* Now interpolate where shifts are spurious */
    if (neighc>0) 
      {
	printf("\nInterpolating using %d neighbours...\n",neighc);
	Interpol((long)NumSources, Rneigh, (float)neighc);
      }
    else
      {
	if (neighc<0) Rneigh=1.0;
	printf("\nInterpolating using fixed radius Rneigh= %f ...\n",Rneigh);
	Interpol_Rfix((long)NumSources, Rneigh);
      }
    free(pixels);
    free(pixels_col);
    
    /* Write shifts */
    for (obj = 1; obj <= NumSources; obj++){
      
      _dx[obj]=_dx[obj]*(float)pixratio; // the X shift (HiRes)
      _dy[obj]=_dy[obj]*(float)pixratio; // the Y shift (HiRes)
      
      // Write shift for this object
      fprintf(log1,"%d %f %f %f %f %d\n",ids[obj],x[obj],y[obj],-_dx[obj],-_dy[obj],interp[obj]);
      
    }
    
    /* Free memory */ 
    free(_dx); free(_dy); free(ids); free(corrweight);
    free(interp);

  }

  fclose(log);fclose(log1);
  return(0);

}

/*------------------------------------------------------------------------------------ */


float CorrFunc(){

  /* Builds the correlation array */

  float mean_ref,mean_col,fac_ref,fac_col, diff;

  valmax=-BIG_REAL;
  
  mean_ref=0.0;
  fac_ref=0.0;
  diff=0.0;
  npix=0;
  
  for (i=yini;i<yend;i++){
    for (j=xini;j<xend;j++){
      //if (pixels[Width*i+j]>threshold){
	mean_ref+=pixels[Width*i+j];
	npix++;
	//}
    }
  }
  mean_ref=mean_ref/(float)npix; 
#ifdef VERBOSE
  printf("npix_ref %d\n",npix);
#endif
  
  for (i=yini;i<yend;i++){
    for (j=xini;j<xend;j++){
      //if (pixels[Width*i+j]>threshold){
	fac_ref+=pow((pixels[Width*i+j]-mean_ref),2);
	//}
    }
  }
  if (fac_ref<=0.0) 
    {fac_ref=1.0;}
  else
    {fac_ref=sqrt(fac_ref);}
  
  jj=0; 
  
  for (ylag=-yshift;ylag<=yshift;ylag++){

    jj++;
    ii=0;
    for (xlag=-xshift;xlag<=xshift;xlag++){
      
      ii++;

      mean_col=0.0;
      fac_col=0.0;
      npix=0;
      for (i=yini;i<yend;i++){
	for (j=xini;j<xend;j++){
	  temp=Width*(i+ylag)+(j+xlag);
	  if ((i+ylag)>=0 && (j+xlag)>=0 && (i+ylag)<Height && (j+xlag)<Width){  
	    //if (pixels[Width*i+j]>threshold){
	      mean_col+=pixels_col[temp];
	      npix++;
	      //}
	  }    
	}
      }
      mean_col=mean_col/(float)npix;
#ifdef VERBOSE
      printf("npix_col %d\n",npix);
      printf("%d %d %d\n",xlag,ylag,npix);
#endif
      diff=0.0;
      for (i=yini;i<yend;i++){
	for (j=xini;j<xend;j++){
	  temp=Width*(i+ylag)+(j+xlag);
	  if ((i+ylag)>=0 && (j+xlag)>=0 && (i+ylag)<Height && (j+xlag)<Width){ 
	    //if (pixels[Width*i+j]>threshold){
	      fac_col+=pow((pixels_col[Width*(i+ylag)+(j+xlag)]-mean_col),2);
	      diff+=(pixels_col[Width*(i+ylag)+(j+xlag)]-mean_col)*(pixels[Width*i+j]-mean_ref);
	      //}
	  }
	}
      }
      if (fac_col<=0.0)
	{fac_col=1.0;}
      else
	{fac_col=sqrt(fac_col);}
      
      val=diff/(fac_ref*fac_col);
      corr[jj][ii] = val;
      if(val>valmax){
	valmax=val;
	imax=ii;
	jmax=jj;
      }	
#ifdef VERBOSE
      printf("> %d %d %g %g %g %g %g %g\n",ii,jj,val,mean_col,mean_ref,fac_ref,fac_col,diff);
#endif
    }   
  }
#ifdef VERBOSE
  printf("MAX: %d %d %f\n",imax,jmax,valmax);
#endif

#ifdef VERBOSE
  jj=0; 
  for (ylag=-yshift;ylag<=yshift;ylag++){
    jj++;
    ii=0;
    for (xlag=-xshift;xlag<=xshift;xlag++){ 
      ii++;
      printf("%f ",corr[jj][ii]);}
    printf("\n");}
#endif

  return(valmax);
  
}


/*------------------------------------------------------------------------------------ */

float CorrFuncFFT(){

  float mean_ref,mean_col,fac_ref,fac_col, diff;
  double *Ref, *Col, *Col0, *mod; 
  int nc, nr, cen, Area;
  int cpos, cneg, rpos, rneg;
  int ai, aj, oi, oj, iii, jjj;

  valmax=-BIG_REAL;
  
  mean_ref=0.0;
  fac_ref=0.0;
  mean_col=0.0;
  fac_col=0.0;
  diff=0.0;
  npix=0;
  
  for (i=yini;i<yend;i++){
    for (j=xini;j<xend;j++){
      mean_ref+=pixels[Width*i+j];
      mean_col+=pixels_col[Width*i+j];
      npix++;
    }
  }
  mean_ref=mean_ref/(float)npix; 
  mean_col=mean_col/(float)npix;

#ifdef VERBOSE
    printf("npix_ref %d\n",npix);
#endif
    
  for (i=yini;i<yend;i++){
    for (j=xini;j<xend;j++){
      fac_ref+=pow((pixels[Width*i+j]-mean_ref),2);
      fac_col+=pow((pixels_col[Width*i+j]-mean_col),2);
    }
  }
  if (fac_ref<=0.0) 
    {fac_ref=1.0;}
  else
    {fac_ref=sqrt(fac_ref);}
  if (fac_col<=0.0)
    {fac_col=1.0;}
  else
    {fac_col=sqrt(fac_col);}  

  /*Now rearrange to convolve. nr and nc are the final dimensions*/
  if (xwindow % 2 == 0) {
    nc=xsize+xwindow/2;}    
  else {
    nc=xsize+(xwindow+1)/2;}
  if (ywindow % 2 == 0) {
    nr=ysize+ywindow/2+1;}    
  else {
    nr=ysize+(ywindow+1)/2;}
  nc = MAX(nc,nr);
  nc = pow(2,ceil(log((double)nc)/log(2)));
  cen = nc/2;
  nr = nc;
  Area = nr*nc;

  Col0 = (double *) malloc(xsize*ysize * sizeof(double));
  Ref = (double *) malloc(Area * sizeof(double));
  Col = (double *) malloc(Area * sizeof(double));
  mod = (double *) malloc(Area * sizeof(double));
  for (ii=0;ii<Area;ii++)
    {Ref[ii]=0.0;Col[ii]=0.0;mod[ii]=0.0;}
  
  // Pad and normalize the data; wrap collage data
  jj=yini-1;
  jjj=0;
  for (j=cen-ysize/2;j<cen-ysize/2+ysize;j++){
    jj++;
    ii=xini-1;
    iii=0;
    for (i=cen-xsize/2;i<cen-xsize/2+xsize;i++){
      ii++;
      Ref[j*nc+i]=(pixels[jj*Width+ii]-mean_ref)/fac_ref;
      Col0[jjj*xsize+iii]=(pixels_col[jj*Width+ii]-mean_col)/fac_col;
      iii++;
    }
    jjj++;
  }

  if (xsize%2==0){
    cpos=xsize/2+1;cneg=cpos-2;
    iii=cen;}
  else{
    cpos=(xsize+1)/2;cneg=cpos-1;
    iii=cen-1;}
  if (ysize%2==0){
    rpos=ysize/2+1;rneg=rpos-2;
    jjj=cen;}
  else{
    rpos=(ysize+1)/2;rneg=rpos-1;
    jjj=cen-1;}
  for (ii=0; ii<rpos; ii++){
    for (jj=0; jj<cpos; jj++){
      oi=rneg+ii;oj=cneg+jj;
      Col[ii*nc+jj]=Col0[oi*xsize+oj];}}
  for (ii=0; ii<rneg; ii++){
    for (jj=0; jj<cpos; jj++){
      ai=nr-rneg+ii;oj=cneg+jj;
      Col[ai*nc+jj]=Col0[ii*xsize+oj];}}
  for (ii=0; ii<rpos; ii++){
    for (jj=0; jj<cneg; jj++){
      oi=rneg+ii;aj=nc-cneg+jj;
      Col[ii*nc+aj]=Col0[oi*xsize+jj];}}
  for (ii=0; ii<rneg; ii++){
    for (jj=0; jj<cneg; jj++){
      ai=nr-rneg+ii;aj=nc-cneg+jj;
      Col[ai*nc+aj]=Col0[ii*xsize+jj];}}
  
  /*
    for (ii=0;ii<ysize;ii++){
    for (jj=0;jj<xsize;jj++){
    printf("%f ",Col0[ii*xsize+jj]);
    }
    printf("\n");
    }
    
    for (ii=0;ii<nr;ii++){
    for (jj=0;jj<nc;jj++){
    printf("%f ",Col[ii*nc+jj]);
    }
    printf("\n");
    }
  */
  
  mod = DoTheConvolutionFFT(Col, Ref, nr, nc); 

    /*for (ii=0;ii<nr;i++){
    for (jj=0;jj<nc;jj++){
      printf("%f ",mod[nc*ii+jj]);
    }
    printf("\n");
    }*/

  imax=0;jmax=0;
  for (ylag=-yshift;ylag<=yshift;ylag++){
    for (xlag=-xshift;xlag<=xshift;xlag++){ 
      i=iii+xlag+1;
      j=jjj+ylag+1;
      ii=xshift+1+xlag;
      jj=yshift+1+ylag;
      val=mod[j*nc+i];
      corr[jj][ii]=val;
#ifdef VERBOSE
      printf("%f ",corr[jj][ii]);
#endif
      if (val>valmax){
	valmax=val;
	imax=ii;
	jmax=jj;
      }
    }
#ifdef VERBOSE
    printf("\n");
#endif
  }

  free(Col); free(Ref); free(mod); free(Col0);
  
#ifdef VERBOSE
  printf("MAX: %d %d %f\n",imax,jmax,valmax);
#endif

  return(valmax);

}


/*------------------------------------------------------------------------------------ */

double* DoTheConvolutionFFT (double *in1, double *in2, int nr, int nc)
{
  int i;
  int nch;
  double *conv;
  fftw_complex *out1,*out2,*out;
  fftw_plan plan_backward;
  fftw_plan plan_forward1, plan_forward2;

  nch = ( nc / 2 ) + 1;
  out = fftw_malloc ( sizeof ( fftw_complex ) * nr*nch );
  out1 = fftw_malloc ( sizeof ( fftw_complex ) * nr*nch );
  out2 = fftw_malloc ( sizeof ( fftw_complex ) * nr*nch );
  plan_forward1 = fftw_plan_dft_r2c_2d ( nr,nc, in1, out1, FFTW_ESTIMATE );
  plan_forward2 = fftw_plan_dft_r2c_2d ( nr,nc, in2, out2, FFTW_ESTIMATE );
  fftw_execute ( plan_forward1 ); fftw_execute ( plan_forward2 );
  
  for (i=0;i<nr*nch;i++){
    out[i][0]=out1[i][0]*out2[i][0]+out1[i][1]*out2[i][1];
    // take the complex conjugate being correlation instead of convolution
    out[i][1]=-(out1[i][0]*out2[i][1]-out1[i][1]*out2[i][0]); 
  }
  conv = fftw_malloc ( sizeof ( double ) * nr*nc );
  plan_backward = fftw_plan_dft_c2r_2d ( nr,nc, out, conv, FFTW_ESTIMATE );
  fftw_execute ( plan_backward );

  for ( i = 0; i < nr*nc; i++ ){ conv[i] = conv[i] / ( double ) ( nr*nc ) ;}
  fftw_destroy_plan ( plan_forward1 );fftw_destroy_plan ( plan_forward2 );
  fftw_destroy_plan ( plan_backward );
  fftw_free ( out1 );fftw_free ( out2 ); fftw_free( out );
  return(conv);

}

/*------------------------------------------------------------------------------------ */

void FindShifts(){

  /* Finds the positions of maximum of correlation function */

  int index;
  float diff;
  
  // Marginals
  index = 1-xlo;
  for (i=xlo;i<=xhi;i++){
    xmarg[index+i]=0.0;
    for (j=ylo;j<=yhi;j++){
      xmarg[index+i] += corr[j][i];
    }
  }
  for (i=1;i<=nx;i++) {xmarg[i]=xmarg[i]/((float)ny);}
  index = 1-ylo;
  for (j=ylo;j<=yhi;j++){
    ymarg[index+j]=0.0;
    for (i=xlo;i<=xhi;i++){
      ymarg[index+j] += corr[j][i];
    }
  }
  for (i=1;i<=ny;i++) {ymarg[i]=ymarg[i]/((float)nx);}
  
  // Compute the shifts
  sumi=0.0; sumix=0.0; mean=0.0;
  for (i=1;i<=nx;i++) mean += xmarg[i];
  mean=mean/((float)nx);
  for (i=1;i<=nx;i++){
    diff=xmarg[i]-mean;
    if (diff>=0.0){
      sumi=sumi+diff;
      sumix=sumix+i*diff;}
  }
  if (sumi!=0.0)
    {xsh=sumix/sumi;} 
  else
    {xsh=(1.0+(float)nx)/2.0;} 
#ifdef VERBOSE
  printf("X: %f %f %f %f\n",sumix,sumi,diff,mean);
#endif

  sumi=0.0; sumix=0.0; mean=0.0;
  for (i=1;i<=ny;i++) mean += ymarg[i];
  mean=mean/((float)ny);
  for (i=1;i<=ny;i++){
    diff=ymarg[i]-mean;
    if (diff>=0.0){
      sumi+=diff;
      sumix+=i*diff;}
  }
  if (sumi!=0.0)
    {ysh=sumix/sumi;} 
  else
    {ysh=(1.0+(float)ny)/2.0;} 

#ifdef VERBOSE
  printf("Y: %f %f %f %f\n",sumix,sumi,diff,mean);
  printf("SHIFTS: %f %f\n",xsh,ysh);
#endif

  xsh = xsh + (float)xlo - xsh/fabs(xsh) - (1.0+(float)xwindow)/2.0;
  ysh = ysh + (float)ylo - ysh/fabs(ysh) - (1.0+(float)ywindow)/2.0;

#ifdef VERBOSE
  printf("SHIFTS final: %ld %ld %ld %ld > %f %f\n",xini, xend, yini, yend, xsh, ysh);
#endif

}


// --------------------------------------------------------------------------- //

void Interpol(long NumSources, float Rneigh, float NNEIGHOK){

  /* Computes shifts for zero-valued cells */ 

  int xxx,yyy;
  float xxxf,yyyf;
  float sumx,sumy,Rneigh2,R2,fac2;
  double *dx, *dy;
  int iter;
  float nneigh;

  dx = dvector(1, NumSources);
  dy = dvector(1, NumSources);

  for (ii=1;ii<=NumSources;ii++){
    interp[ii]=0;
    dx[ii]=_dx[ii];
    if (dx[ii]>999.){
      dx[ii]=0.0;
      interp[ii]+=1;}
    dy[ii]=_dy[ii];
    if (dy[ii]>999.){
      dy[ii]=0.0;
      interp[ii]+=2;}
  }

  for (ii=1;ii<=NumSources;ii++){

    iter=0;
    fac2=1.0;
    Rneigh2=Rneigh*Rneigh;
    do 
      {

	nneigh=0.0;
	Rneigh2=fac2*Rneigh2;

	sumx=0.0;
	xxxf=0.0;
	sumy=0.0;
	yyyf=0.0; 
     
	for (i=1;i<=NumSources;i++){
	  if (interp[i]==0){
	    R2=MAX(1.0,(x[i]-x[ii])*(x[i]-x[ii])+(y[i]-y[ii])*(y[i]-y[ii]));

	    if (R2<=Rneigh2){
	      sumx+=corrweight[i]*dx[i]/sqrt(R2);
	      xxxf+=corrweight[i]/sqrt(R2);
	      sumy+=corrweight[i]*dy[i]/sqrt(R2);
	      yyyf+=corrweight[i]/sqrt(R2);
	      nneigh+=1.0;
	    }
	    
	  }
	}

	fac2=sqrt(NNEIGHOK/nneigh);
	iter+=1;


      } while ((fabs(nneigh-NNEIGHOK)/NNEIGHOK>0.1) && (iter<10));

    if (xxxf>0.0) {
      sumx/=xxxf;
      if (fabs(sumx)>max_shift) {sumx=sumx*max_shift/fabs(sumx);}
      _dx[ii]=sumx;}
    else{
#ifdef VERBOSE
      printf(">>>>>> Interpolation failed %d xxxf=%f\n",ii,xxxf);
#endif
      _dx[ii]=0.0;
    }
    if (yyyf>0.0) {
      sumy/=yyyf;
      if (fabs(sumy)>max_shift) {sumy=sumy*max_shift/fabs(sumy);}
      _dy[ii]=sumy;}
    else{
#ifdef VERBOSE
      printf(">>>>>> Interpolation failed %d yyyf=%f\n",ii,yyyf);
#endif
      _dy[ii]=0.0;
    }
  }
  
  free(dx); free(dy); 
}
  
/* ----------------------------------------------------------------------------------- */


void Interpol_Rfix(long NumSources, float Rneigh){

  /* Computes shifts for zero-valued cells */ 

  int xxx,yyy;
  float xxxf,yyyf;
  float sumx,sumy,Rneigh2,R2,fac2;
  double *dx, *dy;
  int enlarge, nneigh, iter;

  dx = dvector(1, NumSources);
  dy = dvector(1, NumSources);

  for (ii=1;ii<=NumSources;ii++){
    interp[ii]=0;
    dx[ii]=_dx[ii];
    if (dx[ii]>999.){
      dx[ii]=0.0;
      interp[ii]+=1;}
    dy[ii]=_dy[ii];
    if (dy[ii]>999.){
      dy[ii]=0.0;
      interp[ii]+=2;}
  }

  for (ii=1;ii<=NumSources;ii++){

    enlarge=0;
    fac2=1.0;
    nneigh=0;
    xxxf=0.0;
    yyyf=0.0;
    iter=0;

    while (((xxxf<=1.e-5) || (yyyf<=1.e-5)) && (iter<10))
      {
	
	Rneigh2=fac2*Rneigh*Rneigh;

	sumx=0.0;
	xxxf=0.0;
	sumy=0.0;
	yyyf=0.0; 
	
	for (i=1;i<=NumSources;i++){
	  
	  if (interp[i]==0){
	    R2=MAX(1.0,(x[i]-x[ii])*(x[i]-x[ii])+(y[i]-y[ii])*(y[i]-y[ii]));
	    if (R2<=Rneigh2){
	      sumx+=corrweight[i]*dx[i]/sqrt(R2);
	      xxxf+=corrweight[i]/sqrt(R2);
	      sumy+=corrweight[i]*dy[i]/sqrt(R2);
	      yyyf+=corrweight[i]/sqrt(R2);
	      nneigh+=1;
	    }
	    
	  }
	}

     	if (xxxf>1.e-5) {
	  sumx/=xxxf;
	  _dx[ii]=sumx;}
	else{
	  enlarge=1;
#ifdef VERBOSE
	  printf(">>>>>> Interpolation failed %d xxxf=%f\n",ii,xxxf);
#endif
	  _dx[ii]=0.0;
	}
	if (yyyf>1.e-5) {
	  sumy/=yyyf;
	  _dy[ii]=sumy;}
	else{
	  enlarge=1;
#ifdef VERBOSE
	  printf(">>>>>> Interpolation failed %d yyyf=%f\n",ii,yyyf);
#endif
	  _dy[ii]=0.0;
	}
	if (enlarge)
	  {
	    fac2*=2.0;
	    iter+=1;
	  }
      }

    if (iter==10) printf("Interpolation failed for %d, skipping\n",ii);
  }
    
  free(dx); free(dy); 
}
  
/* ----------------------------------------------------------------------------------- */
