
"""
Copyright 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, copy, glob, time, sys
import numpy as N
from astropy import wcs
from astropy.io import fits
from astropy.io import ascii
from subprocess import call
import cull
from shutil import copy

nmaxcat=10000

#---------------------------------------------------------------------------

def TextifyKern(kernel,kerneltxt):
    k=fits.open(kernel)
    kdat=k[0].data
    N.savetxt(kerneltxt,kdat,fmt='%.6e')

#---------------------------------------------------------------------------

class smooth_worker(object):
    def __init__(self,truecutcatfile,kernel,outdir,indir,outcat,loresfile,
                 imgfile,segfile,Xoff,Yoff,scale_factor,FFT,kerntxt,subbckg,
                 mkext,shiftfile,dzonesize):
        self.loresfile = loresfile
        self.imgfile = imgfile
        if not (segfile):
            segfile="None"
        self.segfile = segfile
        self.truecutcatfile = truecutcatfile
        self.kernel = kernel
        self.indir = indir
        self.scale_factor = scale_factor
        self.outdir = outdir
        self.outcat = outcat
        self.Xoff=Xoff
        self.Yoff=Yoff
        self.FFT=FFT
        self.kerntxt=kerntxt
        self.subbckg=subbckg
        self.mkext=mkext
        self.shiftfile=shiftfile
        self.dzonesize=dzonesize
        
    def __call__(self):

        #######
        # Call C code to do convolution. 
        # To parallelize, it will be sufficient to split the input catalog.
        if self.FFT:
            dofft=1
        else:
            dofft=0
        if self.kerntxt:
            txt=1
        else:
            txt=0

        # Very large catalogs (Euclid) might
        # need splitting (who knows why)
        c=open(self.truecutcatfile,'r')
        cc=c.readlines()
        cs=N.size(cc)        
        nnn=0
        o=open(self.outcat,'w')
        o.write('#id x y xmin ymin xmax ymax cutout_meas_flux SEx_flux_best\n')

        while nnn<cs:
            nnn1=min(cs,nnn+nmaxcat)
            tempfile=self.truecutcatfile+'_part'
            o1=open(tempfile,'w')
            for i in range(nnn,nnn1):
                o1.write("%s"%cc[i])
            o1.close()
            tempout=self.outcat+'_part'
            nnn=nnn1

            cmd=' '.join(['tphot_convolve',
                          self.indir,tempfile,self.kernel,
                          self.outdir,tempout, str(self.Xoff), 
                          str(self.Yoff), self.loresfile, '0', 'None', 
                          str(dofft), str(txt), self.imgfile, 
                          self.segfile, str(self.scale_factor), 
                          str(self.subbckg), str(self.mkext), 
                          self.shiftfile, str(self.dzonesize), '1'])

            print cmd
            stat=call(cmd,shell=True)
            if not (stat==0):
                print "ERROR during execution of tphot_convolve routine."
                print "Aborting."
                sys.exit()
                
            o1=open(tempout,'r')
            l1=o1.readlines()
            for line in l1:
                if not line.startswith('#'):
                    o.write(line)
        o.close()
        os.remove(tempfile)
        os.remove(tempout)
         

#---------------------------------------------------------------------------

class smooth_multikern_worker(object):
    # for using multiprocessing in multikern
    def __init__(self,templcat,cutcat,indir,outdir,posframe,LoRes,HiRes,seg,
                 psfgrid,scale_factor,flag,maxflag,FFT,kerntxt,subbckg,mkext,kernel,
                 shiftfile,dzonesize,shifts=None,q_output=None):
        self.templcat = templcat
        self.cutcat = cutcat
        self.indir = indir
        self.outdir = outdir
        self.catcount = 0
        self.posframe = posframe
        self.LoRes = LoRes
        self.HiRes = HiRes
        if not (seg):
            seg="None"
        self.seg = seg
        self.psfgrid = psfgrid
        self.scale_factor = scale_factor
        self.flag = flag
        self.maxflag = maxflag
        self.count = 0
        self.notfound = {}
        self.q_output = q_output
        self.shiftfile = shiftfile
        self.shifts = shifts
        self.FFT=FFT
        self.kerntxt=kerntxt
        self.subbckg=subbckg
        self.mkext=mkext
        self.kernel=kernel
        self.dzonesize=dzonesize
  
    def __call__(self):

        multikern_count={}
    
        mkcatname="multikern/_multikern.txt"
    
        truecutcatfile=self.cutcat

        # Get Xoff and Yoff
        LoResf=fits.open(self.LoRes)
        try:
            Xoff=LoResf[0].header['XOFF']
            Yoff=LoResf[0].header['YOFF']
        except:
            # If required, do the culling here after cutouts
            truecutcatfile=self.cutcat
            #select sources and print new culled catfile
            newcutcatfile=self.cutcat+'_CULLED'
            newcutcat = open(newcutcatfile,'w')
            print "Culling..."
            cull.cull(self.LoRes,self.cutcat,self.HiRes,newcutcat,self.scale_factor,self.kernel,self.kerntxt)
            truecutcatfile=newcutcatfile
            newcutcat.close()
            LoResf=fits.open(self.LoRes)
            Xoff=LoResf[0].header['XOFF']
            Yoff=LoResf[0].header['YOFF']

        if not self.mkext:
            # Save a copy of the single kernel templatecat
            copy(self.templcat, self.templcat+'_pass1')

        kcountot=1

        # Call C code to do convolution from FITS files.
        # To parallelize, it will be sufficient to split the input catalog.
        if self.FFT:
            dofft=1
        else:
            dofft=0
        if self.kerntxt:
            txt=1
        else:
            txt=0

        # Very large catalogs (Euclid) might
        # need splitting (who knows why)
        c=open(truecutcatfile,'r')
        cc=c.readlines()
        cs=N.size(cc)        
        nnn=0
        o=open(self.templcat,'w')
        o.write('#id x y xmin ymin xmax ymax cutout_meas_flux SEx_flux_best\n')

        while nnn<cs:
            nnn1=min(cs,nnn+nmaxcat)
            tempfile=truecutcatfile+'_part'
            o1=open(tempfile,'w')
            for i in range(nnn,nnn1):
                o1.write("%s"%cc[i])
            o1.close()
            tempout=self.templcat+'_part'
            nnn=nnn1

            cmd=' '.join(['tphot_convolve',self.indir,
                          tempfile,self.kernel,self.outdir,tempout,
                          str(Xoff), str(Yoff), self.LoRes, str(kcountot), 
                          mkcatname, str(dofft),str(txt),self.HiRes,self.seg,
                          str(self.scale_factor), str(self.subbckg), 
                          str(self.mkext), str(self.shiftfile),
                          str(self.dzonesize), '1'])
            print cmd
            stat=call(cmd,shell=True)
            if not (stat==0):
                print "ERROR during execution of tphot_convolve routine."
                print "Aborting."
                sys.exit()

            o1=open(tempout,'r')
            l1=o1.readlines()
            for line in l1:
                if not line.startswith('#'):
                    o.write(line)
        o.close()
        os.remove(tempout)
        os.remove(tempfile)

        return self.count,self.catcount,self.notfound
    
#-------------------------------------------------------------------------------------------------------------------


def smooth(indir, cutcat, loresfile, imgfile, segfile, kernel, scale_factor, 
           outdir, templatecat, culling, FFT, kerntxt, subbckg,  
           mkext,dzonesize,shiftfile,nproc=1,fake=None,flagfile=None,maxflag=0,
           savehi=False,hr_outdir=None):
  """
    #Module to process image templates, if stored in FITS files.
    #Limiting assumptions:
    #   Pixels must be aligned on the same grid (a la master GOODS grid)
    #   Pixels must be square
    #   The templates are of a higher resolution & need to be convolved down
    #   The PSF array is square, and at the same pixel resolution as the templates.
    #   The PSF array has been normalized to 1.
    #   The scale factor is an even integer, or 1.
 
  """
  #assert os.path.isdir(indir), "Process: Input directory is not a directory"
  
  assert os.path.exists(kernel), "Process: Kernel file does not exist"
  assert type(scale_factor) == type(5), "Process: Scale factor must be an integer"

  # Check for directories and create them if needed
  if not os.path.isdir(indir):
      os.makedirs(indir)  
  if not os.path.isdir(outdir):
      os.makedirs(outdir)
  if savehi and not os.path.isdir(hr_outdir):
      os.makedirs(hr_outdir)

  # Check if kernel is in txt mode and read it:
  if kerntxt:
      kerneltxt,param=kernel.rsplit(".",1)
      kerneltxt=kerneltxt+".txt"
      try:
          test=ascii.read(kerneltxt)
      except IOError:
          TextifyKern(kernel,kerneltxt)   

  # If required, do the culling here after cutouts
  truecutcatfile=cutcat
  if not culling:
      #select sources and print new culled catfile
      newcutcatfile=cutcat+'_CULLED'
      newcutcat = open(newcutcatfile,'w')
      cull.cull(loresfile,cutcat,imgfile,newcutcat,scale_factor,kernel,kerntxt)
      truecutcatfile=newcutcatfile
      newcutcat.close()
  
  # Get Xoff and Yoff
  LoResf=fits.open(loresfile)
  Xoff=LoResf[0].header['XOFF']
  Yoff=LoResf[0].header['YOFF']  
  worker = smooth_worker(truecutcatfile,kernel,outdir,indir,templatecat,
                         loresfile,imgfile,segfile,Xoff,Yoff,scale_factor,
                         FFT,kerntxt,subbckg,mkext,shiftfile,dzonesize)
  worker.__call__()


#-----------------------------------------------------------------------------------------------

def smooth_multikern(gridfile,indir,outdir,templcat,cutcat,loresfile,HiRes,
                     seg,scale_factor,FFT,kerntxt,subbckg,mkext,kernel,
                     shiftfile,dzonesize,nproc=1,posframe='lowres',flagfile=None,maxflag=0):
    """ Process cutouts through kernels that are shifted one by one for each source
    (based on interpolated shifts measured from the collage). 
    
    Shift file has ID, dx,dy, where the sense of the shift is to add dx,dy to get the 
    desired position of the source in the lores image.
    """

    print "Working on multikernel issues..."
    
    psfgrid='None'
    shifts='None'
    flag=None
    posframe='None'

    #Create the output directory if necessary.  
    if not os.path.isdir(outdir): 
        os.mkdir(outdir)
        
    #Bookkeeping
    notfound={}
    count=0
    catcount=0

    #Clean up old templates
    if not os.path.exists(outdir+'/old/'):
        os.mkdir(outdir+'/old/')
        mvlist=glob.glob('%s/mod-*.fits'%outdir)
        for name in mvlist:
            os.rename(name,"%s/old/%s"%(outdir,os.path.basename(name)))
    else:
        print "Directory storing old templates exists,"
        print "     overwriting new ones only"

    worker = smooth_multikern_worker(templcat,cutcat,indir,outdir,posframe,loresfile,
                                     HiRes,seg,psfgrid,scale_factor,flag,maxflag,FFT,kerntxt,
                                     subbckg,mkext,kernel,shiftfile,dzonesize,shifts)
    count, catcount, notfound = worker.__call__()

    """
    #Report errors
    if len(notfound) > 0:
        print "%d templates could not be found in the lookup table"%len(notfound)
        out=open(gridfile+'_notfound','w')            
        for k in notfound:
            print >> out,k,":",notfound[k]
        out.close()
    """


    return "Of %d templates, %d not found, %d omitted, %d convolved"%(catcount, len(notfound), catcount-count, count)


#-----------------------------------------------------------------------------------------------

             
def template_exists(tname,psfname):
  """ Check not only whether a file of the correct name exists,
  but also whether it was created with a PSF of the correct name """
  try:
    f=fits.open(tname)
    convkern=f[0].header['convkern']
    #if os.path.basename(psfname).lower() == convkern.lower():
    if psfname.lower() == convkern.lower():
      found = True
      #print "template exists with correct kernel"
    else:
      found = False
      os.rename(tname,tname.replace('.fits','_old.fits'))
      #print "template exists with wrong kernel; renamed"
  except IOError:
    found = False

  return found

def template_and_shift_exists(tname,psfname,dx,dy):
  """ Check not only whether a file of the correct name exists,
  but also whether it was created with a PSF of the correct name """
  found = False
  try:
    f=fits.open(tname)
    # Has the template already been convolved with this same kernel?
    convkern=f[0].header['convkern']
    #if os.path.basename(psfname).lower() == convkern.lower():
    if psfname.lower() == convkern.lower():
        found = True
    # Has the template already been shifted by this amount?
    if found: 
      if f[0].header.get('xshift') and f[0].header.get('yshift'): 
        xshift=f[0].header['xshift']
        yshift=f[0].header['yshift']
        if abs(dx-xshift) < 1.e-5 and abs(dy-yshift) < 1.e-5:
           print "%s template exists with correct kernel and shifts" % tname
        else: 
           print "%s template exists but with different shifts (old-new): %6.3f %6.3f: " % (tname,dx-sxhift,dy-yshift)
           found = False
    if not found:
      os.rename(tname,tname.replace('.fits','_old.fits'))
      print "Renaming template %s -> %s " % (tname,tname.replace('.fits','_old.fits'))
  except IOError:
    found = False

  return found
