#!/usr/bin/env python

#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.

def create_cuts(HRI,ZP,modelscat,modelsdir,outcat,outdir,overwrite):

    import math as m

    if not os.path.exists(modelscat):
        print "Catalog does not exist. Aborting"
        sys.exit()
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    modelsdir=modelsdir+'/'
    mvlist=glob.glob(modelsdir+"*.fits")
    if not mvlist:
        print "No models found. Aborting"
        sys.exit()  
    # find the ID
    a=mvlist[0]
    junk,a=a.split(modelsdir,1)
    commonbeg=""
    commonend=""
    for i in range(len(a)):
        if a[i].isdigit():
            break
    if (i>0):
        commonbeg=a[:i-1]
    for j in range(len(a)-1,-1,-1):
        if a[j].isdigit():
            break
    if (j<len(a)-1):
        commonend=a[j+1:]
    print "Changing stamp names; cutting out '",commonbeg,"', '",commonend,"'"
    for name in mvlist:
        junk,newname=name.split(modelsdir,1)
        if commonbeg!='':
            junk,newname=newname.split(commonbeg,1)
        if commonend!='':
            newname,junk=newname.split(commonend,1)
        if "-" in newname: # necessary to avoid strange behaviour... uh?
            newname=newname.replace("-","")
        if not newname.isdigit():
            for i in len(newname):
                if not newname[i].isdigit():
                    newname=newname.replace(newname[i],"")
        newname="cut-"+newname+".fits"
        if overwrite:
            os.rename(name,outdir+'/'+newname)
        else:
            copyfile(name,outdir+'/'+newname)

    # Get some values from HRI header
    ref=fits.open(HRI)
    refh=ref[0].header
    NAXIS1=refh['NAXIS1']
    NAXIS2=refh['NAXIS2']
    CRPIX1ref=refh['CRPIX1']
    CRPIX2ref=refh['CRPIX2']
    CRVAL1ref=refh['CRVAL1']
    CRVAL2ref=refh['CRVAL2']
    CTYPE1ref=refh['CTYPE1']
    CTYPE2ref=refh['CTYPE2']
    CD1_1ref=refh['CD1_1']
    CD2_2ref=refh['CD2_2']
    ref.close()

    lll=glob.glob(outdir+"/cut-*fits")

    out=open(outcat,'w')
    out.write('#id x y xmin ymin xmax ymax detbckg flux\n')
    components=1
    if "_1" in modelscat:
        cats,junk=modelscat.split("_1",1)
        components=int(n.size(glob.glob(cats+"_*")))
        print "Number of components:",components
    else:
        cats=modelscat
    for ccc in range(components):
        if components>1:
            co="_"+str(ccc+1)
            a=ascii.read(cats+co)
        else:
            a=ascii.read(cats)
        for line in a:
            id=int(line[line.colnames[0]])
            
            idstamp=outdir+"/cut-"+str(id)+".fits"
            if idstamp in lll:
                xmin=line[line.colnames[3]]+1 #1
                ymin=line[line.colnames[4]]+1 #1
                xmax=line[line.colnames[5]]+1 #1
                ymax=line[line.colnames[6]]+1 #1 
                x=line[line.colnames[1]]+line[line.colnames[3]] -1.
                y=line[line.colnames[2]]+line[line.colnames[4]] -1.
                bkgd=line[line.colnames[7]]
                flux=line[line.colnames[8]]
                #flux=pow(10.0,0.4*(ZP-line[line.colnames[8]]))
                cutt=False
                xxmin=xmin
                yymin=ymin
                xxmax=xmax-xmin+1
                yymax=ymax-ymin+1
                if(xmin<1):
                    #print id,'xmin',line[line.colnames[3]],'1'
                    xxmin=1-xmin
                    xmin=1
                    cutt=True
                if(ymin<1):
                    #print id,'ymin',line[line.colnames[4]],'1'
                    yymin=1-ymin
                    ymin=1
                    cutt=True
                if(xmax>NAXIS1):
                    #print id,'xmax',line[line.colnames[5]],NAXIS1
                    xxmax=1-line[line.colnames[3]]+NAXIS1
                    xmax=NAXIS1
                    cutt=True
                if(ymax>NAXIS2):
                    #print id,'ymax',line[line.colnames[6]],NAXIS2
                    yymax=1-line[line.colnames[4]]+NAXIS2
                    ymax=NAXIS2
                    cutt=True
                if cutt:
                    cut=outdir+"/cut-"+str(id)+".fits"
                    print "\rResizing stamp",id
                    if os.path.exists(cut):
                        f=fits.open(cut,mode='update') 
                        f[0].data=f[0].data[yymin:yymax+1,xxmin:xxmax+1]
                        f.flush()
                        f.close()
                out.write("%d\t%f\t%f\t%d\t%d\t%d\t%d\t%f\t%f\n"%(id,x,y,xmin,ymin,xmax,ymax,bkgd,flux))
            else:
                print "\rNo stamp found for object ID",id,"; skipping",
    out.close()

    print "Modifying stamps headers:"
    l=ascii.read(outcat)
    count=0
    for line in l:  
        count+=1
        i=int(line[line.colnames[0]])
        cut=outdir+"/cut-"+str(i)+".fits"
        print "\rWorking on stamp",count,"ID:",i,"name:",cut,
        if os.path.exists(cut):
            f=fits.open(cut,mode='update')
            f[0].header['CRPIX1']=CRPIX1ref-line[line.colnames[3]] +1.
            f[0].header['CRPIX2']=CRPIX2ref-line[line.colnames[4]] +1.
            f[0].header['CRVAL1']=CRVAL1ref
            f[0].header['CRVAL2']=CRVAL2ref
            f[0].header['CTYPE1']=CTYPE1ref
            f[0].header['CTYPE2']=CTYPE2ref        
            f[0].header['CD1_1']=CD1_1ref
            f[0].header['CD2_2']=CD2_2ref
            f[0].header['CONVKERN']='None'
            f[0].header['FLUXNORM']='T'
            f[0].header['TOTFLUX']=1.0
            s=n.sum(f[0].data)
            f[0].data=f[0].data/s
            f.flush()
            f.close()
        else:
            print "\nWARNING: Source ID %d is in catalog but the stamp does not exist. Skipping"%i

if __name__ == '__main__':
    """
    This module converts Galfit stamps and catalog 
    into TPHOT readable cutouts and catalog
    """

    import os,glob,sys
    import numpy as n
    from astropy.io import fits
    from astropy.io import ascii
    from shutil import copyfile
    
    if n.size(sys.argv)!=7 and n.size(sys.argv)!=2:
        print "USAGE: galfit2tphot HRI ZeroPoint modelscat modelsdir outcat outdir"
        sys.exit()
    if sys.argv[1]=="-h":
        print "USAGE: galfit2tphot HRI ZeroPoint modelscat modelsdir outcat outdir"
        sys.exit()

    HRI=sys.argv[1]
    ZP=float(sys.argv[2])
    modelscat=sys.argv[3]
    modelsdir=sys.argv[4]
    outcat=sys.argv[5]
    outdir=sys.argv[6]

    if (outdir==modelsdir):
        print "Converting models listed in %s and stored in %s; OVERWRITING"%(modelscat,modelsdir)
        overwrite=True
    else:
        print "Converting models listed in %s and stored in %s; copying in %s"%(modelscat,modelsdir,outdir)
        overwrite=False
    create_cuts(HRI,ZP,modelscat,modelsdir,outcat,outdir,overwrite)
    
    print
    print "Done, bye."

    
