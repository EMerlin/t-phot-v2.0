
#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.

import sys
import numpy as np
from astropy import wcs
from astropy.io import fits

def GetOffset(LoResFile,imgfile):
    #Get the lores wcs infos 
    LoRes=fits.open(LoResFile,mode='update')
    LRupX=LoRes[0].shape[1]
    LRupY=LoRes[0].shape[0] 
    LRdownX=1 
    LRdownY=1 
    LoResPix=np.array([LRdownX,LRdownY,LRupX,LRupY])
    LoResPix.shape=(2,2)
    LRw=wcs.WCS(LoRes[0].header)
    LRWBi=LRw.wcs_pix2world(LoResPix,0) 
    # LRWBi contains lower and upper WCS values

    #Get the hires wcs infos     
    HiRes=fits.open(imgfile)
    HRw=wcs.WCS(HiRes[0].header)

    # Get LoRes infos in HiRes reference
    # lim contains meas image pixels limits in the HiRes pixel
    limf=HRw.wcs_world2pix(LRWBi,0)    

    Xoff=int(round(limf[0][0])) # Xoff
    Yoff=int(round(limf[0][1])) # Yoff

    print "Xoff:", Xoff, ", Yoff:", Yoff
    print "Bye"

if __name__ == '__main__':
    loresfile, hiresfile = sys.argv[1:]
    GetOffset(loresfile, hiresfile)
