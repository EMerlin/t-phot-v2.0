
#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.

from astropy.io import ascii,fits
import glob,shutil
import numpy as np
import random,sys,os
from subprocess import call

def bkgd_templ(shap,idoff,templdir,templcat):

    print "Preparing background templates (shape: %s)"%shap

    if shap=='c':
        sh=1
    else:
        sh=0

    cmd=' '.join(['tphot_make_bkgd_templates',templdir,templcat,str(idoff),str(sh)]);
    print cmd
    call(cmd,shell=True)

    # Merge catalogs
    cmd=' '.join(['cp',templcat,templdir+'/_templates_only.cat'])
    call(cmd,shell=True)    
    cmd=' '.join(['cat',templcat,templdir+'/_templates_bkgd.cat','> temp.txt'])
    call(cmd,shell=True)
    cmd=' '.join(['mv temp.txt',templcat])
    call(cmd,shell=True)

    print "\nDone"



if __name__ == '__main__':
    
    if np.size(sys.argv)!=5:
        print "USAGE: python make_bkgd_templates.py c/s (c=circular, s=square) idoff templdir templcat"
        sys.exit()
        
    shap=sys.argv[1]
    idoff=int(sys.argv[2])
    templdir=sys.argv[3]
    templcat=sys.argv[4]

    bkgd_templ(shap,idoff,templdir,tempcat)
