import math
## def myround(number,epsilon=0.0001):
##     sign = number/abs(number)
##     if abs( (number%1 - 0.5) ) < epsilon:
##        number=sign*int(abs(number))+1
##     else:
##        number=int(round(number))
##     return number

class NonOverlapError(ValueError):
    """Raise if operation requires overlapping boxes but boxes
    do not overlap."""
    pass

def halfround(number,epsilon=0.0001):
    frac, full = math.modf(number)
    if abs(abs(frac)-0.5)<epsilon:
	frac=0.5 * (frac/abs(frac))
    elif abs(frac) < epsilon:
	frac=0.0
    return full+frac

def pixel_index(number, corner='lower'):
   """ Returns a box with the pixel-indices as the vertices,
   using the FITS pixel convention that the center of the first
   pixel has coordinate 1.0. 
       In order to get the right answer when converting from local_box
   to wcs_box, we treat half-pixels N.5 as belonging to pixel N+1
   by default. The optional 'corner' keyword permits the user to
   resolve the ambiguity by defining that a position of N.5 in the 
   lower left corner belongs to pixel  N+1, and in the upper right 
   corner belongs to pixel N. """
   upper=['upper','ux','uy']
   lower=['lower','lx','ly']
   number=halfround(number)
   if corner in upper:
       return int(math.floor(number))
   elif corner in lower:
       return int(math.ceil(number))

class Box:
   """ 
    // A simple class to facilitate manipulating ranges.
    // Note that lx,ly,ux,uy are *pixel coordinates*
    // and they start counting from ONE.
    // Thus a 10x10 box will have corners 1,10,1,10, and npix will be 100."""

   def __init__(self, x1=0, y1=0, x2=0, y2=0):
       self.lx=x1
       self.ly=y1
       self.ux=x2
       self.uy=y2
       self.npix = self.Xdim()*self.Ydim()

   def __repr__(self):
       return "\n lx,ly = ["+repr(self.lx)+","+repr(self.ly)+"] \n"+ \
              " ux,uy = ["+repr(self.ux)+","+repr(self.uy)+"] \n"+ \
	      " npix  = " + repr(self.npix)

   def __add__(self,number):
       return Box(self.lx+number, self.ly+number,
		  self.ux+number, self.uy+number)

   def __sub__(self,number):
       return Box(self.lx-number, self.ly-number,
		  self.ux-number, self.uy-number)

   def __mul__(self,factor):
       return Box(self.lx*factor, self.ly*factor,
		  self.ux*factor, self.uy*factor)


   def __div__(self,factor):
       return Box(self.lx/factor, self.ly/factor,
		  self.ux/factor, self.uy/factor)

   def corners(self):
       return [self.lx, self.ly, self.ux, self.uy]


   def int(self):
       """ Forces integer values """
       self.lx = int(self.lx); self.ly = int(self.ly)
       self.ux = int(self.ux); self.uy = int(self.uy)
       self.npix = self.Xdim()*self.Ydim()  
 

   def round(self):

       """ Rounds vertices to nearest integer. Can be used to return the
       pixel index of the pixels containing the vertices, assuming the
       usual FITS convention is followed that (1,1) is the center of
       pixel."""
	       
##        self.lx = myround(self.lx)  #int(round(self.lx))
##        self.ly = myround(self.ly)  #int(round(self.ly))
##        self.ux = myround(self.ux)  #int(round(self.ux))
##        self.uy = myround(self.uy)  #int(round(self.uy))

       self.lx = int(round(self.lx))
       self.ly = int(round(self.ly))
       self.ux = int(round(self.ux))
       self.uy = int(round(self.uy))

       self.npix = self.Xdim()*self.Ydim()

   def cornerpuff(self):
       """ 'Puffs' the box out to the outermost corners, assuming
       that the original box represents pixel indices. """
       self.lx -= .5 ; self.ly -= .5
       self.ux += .5 ; self.uy += .5
       self.npix = self.Xdim()*self.Ydim()

   def centerpull(self):
       """ Inverse function to centerpuff(). Assuming the outermost
       corners are specified, returns the integer pixel values, ie,
       the positions of the pixel centers. """
##        self.lx = int(self.lx) + 1
##        self.ly = int(self.ly) + 1
##        self.ux = int(self.ux)
##        self.uy = int(self.uy)
       self.lx = int(halfround(self.lx) + 0.5)
       self.ly = int(halfround(self.ly) + 0.5)
       self.ux = int(halfround(self.ux) - 0.5)
       self.uy = int(halfround(self.uy) - 0.5)

       self.npix = self.Xdim() * self.Ydim()
##        if self.type == 'cornerpuff':
## 	   self.type = 'index'

   def Slice(self):
       """ Returns a tuple of slices in python numarray pixel conventions. """
       return ( slice(int(self.ly-1), int(self.uy)), \
		slice(int(self.lx-1), int(self.ux))  )

   def LocalSlice(self,other):
       """Returns the slice corresponding to "other", but in the ref frame 
       of "self"."""
       # - 1 on ly/lx term
       #print "here we are in localslice"
       return( slice(int(other.ly)-int(self.ly), int(other.uy)-int(self.ly)), \
	       slice(int(other.lx)-int(self.lx), int(other.ux)-int(self.lx))  )

## Superseded by new Image class
##    def FitsImage(self, hdr):
##        self.lx = hdr['crpix1']
##        self.ly = hdr['crpix2']
##        self.ux = self.lx + hdr['naxis1']
##        self.uy = self.ly + hdr['naxis2']
##        self.npix = self.Xdim()*self.Ydim()
   
   def Xdim(self):
       if self.ux != self.lx: 
	   return self.ux-self.lx+1
       else:
	   return 0

   def Xrange(self):
       return (self.lx, self.ux)

   def Ydim(self):
       if self.uy != self.ly:
	   return self.uy-self.ly+1
       else:
	   return 0

   def Yrange(self):
       return (self.ly, self.uy)

   def Move(self,xdelta, ydelta):
       self.lx += xdelta
       self.ly += ydelta
       self.ux += xdelta
       self.uy += ydelta

   def Includes(self, xx, yy):
       return (xx >= self.lx and xx <= self.ux  \
	   and yy >= self.ly and yy<= self.uy)

   def Contains(self, thatbox):
        return (self.Includes(thatbox.lx, thatbox.ly) and \
	        self.Includes(thatbox.ux, thatbox.uy) )

   def Overlaps(self, thatbox):
       "NB: This means a partial overlap."
       if (self.Contains(thatbox) or thatbox.Contains(self)):
	   return 0
       else:
	   return (self.Includes(thatbox.lx, thatbox.ly) or \
		   self.Includes(thatbox.ux, thatbox.ly) or \
		   self.Includes(thatbox.lx, thatbox.uy) or \
		   self.Includes(thatbox.ux, thatbox.uy) or \

		   thatbox.Includes(self.lx, self.ly) or \
		   thatbox.Includes(self.ux, self.ly) or \
		   thatbox.Includes(self.lx, self.uy) or \
		   thatbox.Includes(self.ux, self.uy)    )

   def GrowsToHold(self,thatbox):
       if (self.lx > thatbox.lx): self.lx=thatbox.lx
       if (self.ux < thatbox.ux): self.ux=thatbox.ux
       if (self.ly > thatbox.ly): self.ly=thatbox.ly
       if (self.uy < thatbox.uy): self.uy=thatbox.uy
       self.npix=self.Xdim()*self.Ydim()

   def ShrinksToFit(self,thatbox):
       if (self.lx < thatbox.lx): 
	   self.lx=thatbox.lx
       if (self.ux > thatbox.ux): 
	   self.ux=thatbox.ux
       if (self.ly < thatbox.ly): 
	   self.ly=thatbox.ly
       if (self.uy > thatbox.uy): 
	   self.uy=thatbox.uy
       self.npix=self.Xdim()*self.Ydim()

   def Intersection(self,thatbox):
       if (self.Overlaps(thatbox) or
           self.Contains(thatbox) or
           self.IsInside(thatbox)):
           lx=max(self.lx,thatbox.lx)
           ly=max(self.ly,thatbox.ly)
           ux=min(self.ux,thatbox.ux)
           uy=min(self.uy,thatbox.uy)
           return Box(lx,ly,ux,uy)
       else:
           raise NonOverlapError

   def IsInside(self,thatbox):
       return (thatbox.Includes(self.lx,self.ly) and thatbox.Includes(self.ux,self.uy))

   def shape(self):  #Is this a good idea? or return (x,y)? or make a flag?
       return ( self.Ydim(),self.Xdim() )

   def embed(self, border):
       """ Add a border round the perimeter of the box. """
##        lx=self.lx-xborder ; ly = self.ly-yborder
##        ux=self.ux+xborder ; uy = self.uy+yborder
       lx=self.lx-border ; ly = self.ly-border
       ux=self.ux+border ; uy = self.uy+border
 
       return Box(x1=lx,x2=ux,y1=ly,y2=uy)

	      
