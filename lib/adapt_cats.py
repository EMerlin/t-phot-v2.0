from astropy.io import ascii
import numpy as np
import sys, csv

"""
Copyright 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Script to make output LRI catalog the same length as any input catalog
adding fake entries for missing sources
"""

def adapt(hrcat,LRIcat,OUTcat,Nhead):

    fff=open(LRIcat,'r')
    lines=fff.readlines()
    out=open(OUTcat,'w')

    out.write("#  1   ObjectID\n#  2   X\n#  3   Y\n#  4   Cell \n#  5   cx          X pos in cell \n#  6   cy          Y pos in cell \n#  7   Rcell \n#  8   FitQty      Fit Quantity (Flux or Scale Factor)\n#  9   FitQuErr    Variance of Fit Quantity\n# 10   SExF        SExtractor flux_best in hires catalog\n# 11   Totflux     Total flux in hires cutout\n# 12   Flag       Flag for saturation and/or prior blending\n# 13   NumFits     Number of fits for this object\n# 14   MaxFlag     Max flag value in object template\n")
    if Nhead==16:
        out.write("#  15  MaxCvID\n#  16  MaxCvRatio\n")
    

    #hrcat=ascii.read(HRIcat)
    lrcat=ascii.read(LRIcat)
    columns=np.size(lrcat.colnames)
    IDsLR=list(lrcat[lrcat.colnames[0]])

    sniffer = csv.Sniffer()
        
    for lll in hrcat:
        while (lll[0]==' ') or (lll[0]==''):
            lll=lll[1:]
        dialect = sniffer.sniff(lll)
        sep=dialect.delimiter
        
        try:
            id,x,y,junk=lll.split(sep,3)
        except:
            id,x,y=lll.split(sep,2)
        id=int(id)
        x=float(x)
        y=float(y)
        
        if id in IDsLR:
            idx=IDsLR.index(id)
            out.write(lines[idx+Nhead])
        else:
            out.write("%d %f %f 0 0.0000 0.0000 0.0000 -0.990000E+02 0.990000E+10 0.0 0.0 100 0 -99"%(id,x,y))
            if Nhead==16:
                out.write(" 0 99.0000")
            out.write("\n")

    out.close()

    return(0)
