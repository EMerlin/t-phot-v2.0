
#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division
import os,sys
from shutil import copyfile
import time
import math as m
import numpy as np
from astropy import wcs
from astropy.io import fits

def cull(LoResFile,catfile,imgfile,newcat,scale_factor,kernel,txt):

    print "Culling; Starting time: ",time.time()
    if not os.path.exists(imgfile):
        print "No HiResImage given, skipping culling"
        copyfile(catfile,catfile+'_CULLED')
        return
    assert os.path.exists(LoResFile), "Cull: LoResFile %s does not exist"%LoResFile
    assert os.path.exists(catfile), "Cull: catfile %s does not exist"%catfile

    #TTD: insert use of lores weightmap, to set weight_has_good_data

    #Get the kernel infos   
    if (txt):
        kern=np.loadtxt(kernel)
        Width=kern.shape[0]
        Height=kern.shape[1]
    else:
        h=fits.Header.fromfile(kernel)
        Width=h['NAXIS1'] 
        Height=h['NAXIS2'] 

    #Get the lores wcs infos 
    LoRes=fits.open(LoResFile,mode='update')
    LRupX=LoRes[0].shape[1]+.5 # +.5 because first pixel is CENTERED on 1.0 and we need the borders
    LRupY=LoRes[0].shape[0]+.5
    LRdownX=0.5 
    LRdownY=0.5 
    LoResPix=np.array([LRdownX,LRdownY,LRupX,LRupY])
    LoResPix.shape=(2,2)
    LRw=wcs.WCS(LoRes[0].header)
    LRWBi=LRw.wcs_pix2world(LoResPix,1)
    # LRWBi contains lower and upper WCS values
    
    #Get the hires wcs infos     
    HiRes=fits.open(imgfile)
    HRw=wcs.WCS(HiRes[0].header)

    # Get LoRes infos in HiRes reference
    # limf contains meas image pixels reference in the HiRes pixel ref
    limf=HRw.wcs_world2pix(LRWBi,1)    

    Xoff=int(round(limf[0][0]-.5))
    Yoff=int(round(limf[0][1]-.5))

    Xmin=Xoff+1
    Ymin=Yoff+1
    Xmax=Xmin+scale_factor*LoRes[0].header['NAXIS1']-1
    Ymax=Ymin+scale_factor*LoRes[0].header['NAXIS2']-1
    
    # Write Xoff and Yoff to LoRes Image Header
    print "Xoff, Yoff: ", Xoff, Yoff
    
    LoRes[0].header['XOFF']=(Xoff,'HR Pixel offset wrt HRI on the X axis')
    LoRes[0].header['YOFF']=(Yoff,'HR Pixel offset wrt HRI on the Y axis')
    LoRes.flush()
    
    weight_has_good_data = 1  #Insert WtMapFile,WtMap later

    #Read the catalog
    oldcat=open(catfile,'r') #.readlines()

    #Go through each object & determine whether to keep as it is, change, or cull
    tot, ncount = 0, 0
    for line in oldcat:
        line=line.strip() #trim whitespace
        if not line.startswith('#'):
            tot=tot+1
            eliminate = False
            id,x,y,xmin,ymin,xmax,ymax,detbckg,measbckg = line.split(None,9)
            x=float(x)
            y=float(y)
            xmin=int(float(xmin))
            ymin=int(float(ymin))
            xmax=int(float(xmax))
            ymax=int(float(ymax))
            rows=ymax-ymin+1
            columns=xmax-xmin+1

            # Check if thumbnail is *completely* outside LoResBox
            #No more need to use WCS again; use pixel reference ;) 

            if (Width>0 or Height>0):
                # Now enlarge the limits of the thumbnail to consider its
                # dimensions AFTER convolution (in HR pixel scale and LR ref)
                
                nc = columns+Width+1
                nr = rows+Height+1
                
                xminc=m.floor(x-float(nc/2))-1 
                xmaxc=xminc+nc+1 
                yminc=m.floor(y-float(nr/2))-1 
                ymaxc=yminc+nr+1 
 
            else:

                xminc=xmin
                yminc=ymin
                xmaxc=xmax
                ymaxc=ymax

            flag=0
            if ((xmaxc<Xmin) or (xminc>Xmax)):
                #print 'a',id,xminc,Xmin,xmaxc,Xmax
                eliminate=True
            elif ((ymaxc<Ymin) or (yminc>Ymax)):
                #print 'b',id,yminc,Ymin,ymaxc,Ymax
                eliminate=True


            if not eliminate:
                nline="%s %f %f %d %d %d %d %s %s\n"%(id,x,y,xmin,ymin,xmax,ymax,detbckg,measbckg)
                newcat.write(nline)
                ncount=ncount+1 
            else:
                pass #print "Object %s culled out" %(id)

    oldcat.close()
    print "Objects kept/removed: %d/%d"%(ncount,tot-ncount)
    print "Finishing time: ", time.time()
    if ncount<1:
        print "No object kept! Check data... Aborting."
        sys.exit()



