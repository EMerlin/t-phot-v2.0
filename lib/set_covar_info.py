#!/usr/bin/python

#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.


import covar_proc, catutils
import sys, os
from astropy. io import fits
import glob


def get_covar_from_fits(fname):
    f=fits.open(fname)
    cell=covar_proc.covarcell(f[0].header['cellid'],
                              f[1].data.field('object'),
                              f[0].data)
    f.close()
    return cell

def covar(catname,covardir):
    print "Starting diagnostic stages..."
    cat=catutils.tphot_catalog(catname)
    cat.addemptycolumn('MaxCvID','Int32')
    cat.addemptycolumn('MaxCvRatio','Float32')
    for i in cat:
        
        objid=int(cat.objectid[i])
        cellid=int(cat.cell[i])        
        fname=covardir+'/'+str(cellid)+'.fits'
        mycell=get_covar_from_fits(fname)
        cat.MaxCvRatio[i], cat.MaxCvID[i] = mycell.covarworst(objid)

    cat.writeto(catname+'_plus_covar', clobber=True)

if __name__ == '__main__':
    catname, covardir = sys.argv[1:]
    covar(catname,covardir)


