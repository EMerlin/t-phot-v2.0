import copy
import box
import wcs
from astropy.io import fits

True  = 1
False = 0

class Image:
    """ A composite object designed to manage all necessary pixel transformations,
    both between images, and between FITS conventions and Python conventions, 
    inside methods to hide the complexity from the user, and free the user to
    think about the problem instead of the details."""

    def __init__(self, filename, ext=0):
	""" Creates a new Image object from a FITS file. Multi-extension
	files are supported. Attributes are populated from the header."""
	self.filename = filename+"["+`ext`+"]"
        self.f = fits.open(filename)
	self.h = self.f[ext].header
	self.d = self.f[ext].data
	self.wcs = wcs.WCS(self.h)
        if self.d is not None:
            self.local_box = box.Box(1,1,self.h['naxis1'],self.h['naxis2'])
            self.wcs_box   = self.wcs.wcspos(self.local_box)
	    #self.frame = self.wcs
	self._changed = False


    def __str__(self):
	longstring="\n "
	for k in self.__dict__:
	    if not k.startswith('_'): #Hide the hidden attributes
		longstring+= k +" : "+ str(self.__dict__[k])+ "\n"
	return longstring


    def ShrinksToFit(self, other):
	""" Operates on the wcs, wcs_box, and local_box of self in order
	to effectively "trim" this image to fit inside the other."""
	#Get the piece that overlaps
	keeper=other.wcs_box.Intersection(self.wcs_box)
	#Copy & transform to local coords
	newlocal = self.wcs.localpos(keeper)
	newlocal.round() #To be sure it's pixel indices:should be "round"?
	#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	#Move the wcs - only if necessary. I think this sign is correct now.cu
	self.wcs.xyshift(self.wcs_box.lx-keeper.lx, \
			 self.wcs_box.ly-keeper.ly )
	#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	#Take only the data we want
	self.wcs_box = keeper
	self.local_box = newlocal
	self.d = self.d[self.local_box.Slice()]
	#Update the header
	self.wcs.insert(self.h)
	self.h['naxis1'], self.h['naxis2'] = self.local_box.shape()
	#That's it.

    def xypos(self,other):
	""" Returns a box representing the location of the "other" image
	in the xyframe of the "self" image. """
	lx,ly = self.wcs.xy2xy(other.wcs_box.lx, other.wcs_box.ly, other.wcs)
	ux,uy = self.wcs.xy2xy(other.wcs_box.ux, other.wcs_box.uy, other.wcs)
	newbox = box.Box(lx,ly,ux,uy)
	

    def updateto(self,filename,clobber=False):
	import os   
	if self._changed:
	    if not clobber:
		root, ext = os.path.splitext(self.filename)
		tempname = root+'_old'+ext
		os.rename(self.filename,tempname)
	    self.f.writeto(filename)

    def writeto(self,filename):
	self.f[0].header = self.h
	self.f[0].data   = self.d
	self.f.writeto(filename)
				

