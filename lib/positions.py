
"""
Copyright 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import numpy as np
from subprocess import call
from astropy.io import ascii

def create_cat(incat,psffile,outdir,templatecat,loresfile,merge,shiftfile,pixratio,mkext):

    """
    Takes an XY input catalog and prepares the catalog in the
    right format for fitting routine. Input catalog is in LRI frame.
    PSF must be in LRI pixel scale
    E. Merlin 5/2014
    """

    if not os.path.isdir(outdir):
        os.mkdir(outdir)

    trueincat=incat
    pixratio=float(pixratio)

    #Check if additional shifting from previous dance stage is present
    if shiftfile!=None:
        #Read in shift file
        s=ascii.read(shiftfile)
        ids=s['id']
        IDs=ids.tolist()
        x=s['cx']
        y=s['cy']
        xs=s['xshift']
        ys=s['yshift']

        cat=ascii.read(incat)
        trueincat=incat+'_danced'
        if os.path.exists(trueincat):
            os.remove(trueincat)
        out=open(trueincat,'w')
        numsources=np.size(cat[cat.colnames[0]])
        for j in range(numsources):
            ido=cat[cat.colnames[0]][j]
            try:
                idx=IDs.index(ido)
                out.write("%s\t%f\t%f\n"%(cat[cat.colnames[0]][j],x[idx]+xs[idx]/pixratio,y[idx]+ys[idx]/pixratio))
            except:
                out.write("%s\t%f\t%f\n"%(cat[cat.colnames[0]][j],0.0,0.0))
            
        out.close()

    cmd=' '.join(['tphot_psf',
                  outdir,templatecat,psffile,
                  loresfile,trueincat,str(mkext)])
    print cmd
    stat=call(cmd,shell=True)
    if not (stat==0):
        print "ERROR during execution of tphot_psf routine."
        print "Aborting."
        sys.exit()

    # Merge resolved and unresolved catalogs if necessary
    if merge:
        cat1,junk=templatecat.split("_psf",1)
        cat2=templatecat
        o=open("merged_temp.cat",'w')
        c1=open(cat1,'r')
        c2=open(cat2,'r')
        t=c1.readlines()
        for line in t:
            o.write(line)
        for line in c2:
            if not line.startswith("#"):
                o.write(line)
        o.close()
        os.rename("merged_temp.cat",cat1)
        
