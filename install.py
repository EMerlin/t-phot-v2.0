#! /usr/bin/env python

""" This script installs T-PHOT on your machine """

import os,sys,shutil
from subprocess import call

try:
    if os.path.exists("bin"):
        shutil.rmtree("bin")
    os.makedirs("bin")
    os.chdir("lib/src")
    os.chdir("fitter_src")
    cmd="make"
    stat=call(cmd,shell=True)
    if stat:
        print "Failed compiling tphot_fitter. Aborting"
        sys.exit()
    os.chdir("..")
    os.chdir("c_srcs")
    stat=call(cmd,shell=True)
    if stat:
        print "Failed compiling tphot C codes. Aborting"
        sys.exit()
except Exception, e:
    print "ERROR during compilation. Aborting"
    sys.exit()

try:
    os.chdir("../../../")
    os.chdir("bin")
    os.symlink("../lib/tphot.py","tphot")
    os.symlink("../lib/set_covar_info.py","set_covar_info")
    os.symlink("../lib/galfit2tphot.py","galfit2tphot")
    os.symlink("../lib/tphot_cutout","tphot_cutout")
    os.symlink("../lib/tphot_convolve","tphot_convolve")
    os.symlink("../lib/tphot_fitter","tphot_fitter")
    os.symlink("../lib/tphot_dance","tphot_dance")
    os.symlink("../lib/tphot_psf","tphot_psf")
    os.symlink("../lib/tphot_make_bkgd_templates","tphot_make_bkgd_templates")
    os.symlink("../lib/tphot_fakesources.py","tphot_fakesources.py")

    print 
    print "TPHOT succesfully installed, have fun!"
except Exception, e:
    print "ERROR during linking. Aborting"
    sys.exit()
