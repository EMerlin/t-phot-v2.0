# TPHOT PARAMETER FILE
# It is STRONGLY recommended to use give meaningful
# filenames to all output files and directories, indicating
# the dataset(s) from which they come.
# See below for parameters meaning.

#-------------------------- PIPELINE ------------------------------#
# Select the desired one or create another:
# 1st PASS:
order     cutout, convolve, fit, diags, dance, plotdance, archive

#------------------------ CUTOUT STAGE ----------------------------#

usereal		true
useunresolved	false
usemodels	false

hiresfile       HRI.fits
hiresseg        HRI.seg.fits
hirescat        HRI.cat

culling		true

hicatcolnames   None
relscale        1

cutoutdir       cutouts
cutoutcat       cutouts/_cutouts.cat
normalize       true

#------------------------ CONVOLUTION STAGE -----------------------#

loresfile       LRI.fits
loreserr        LRI.rms.fits
errtype         rms
rmsconstant     1
bgconstant      0

#loresflag      lores_flag.fits
maxflag         64            

FFTconv		true
multikernels	false
kernelfile      kernel.fits

posframe        hires
#kerntxt	true

templatedir     templates
templatecat	templates/_templates.cat
#save_hr        True
#hr_templatedir hr_templates

#------------------------ FITTING STAGE ----------------------------#

# Filenames:
fitpars	         tpipe_tphot.param
tphotcat         lores_tphot.cat_pass1
tphotcell        lores_tphot.cell_pass1
tphotcovar       lores_tphot.covar_pass1

# Control parameters:
fitting		single

dithercell      true
cellmask        true
maskfloor       1e-9
fitbackground   false
writecovar      true

threshold	0.0
linsyssolver	lu # [options: ibg, cholesky, lu] 
clip		false

#------------------------- DIAGNOSTICS STAGES ----------------------#

modelfile       lores_collage_pass1.fits

# Dance:
dzonesize       20
maxshift        1.0
nneighinterp	0
ddiagfile       ddiags.txt
dlogfile        dlog.txt
dancefft	true


#....................................................................
#....................................................................


################# PARAMETERS MEANING ###############################
#------------------------- PIPELINE -------------------------------#
# Typical firstpass run:
# order   cutout, convolve, transform, cull, fit, diags, dance, plotdance
# The typical firstpass run contains all the valid stages.
# There are several possible paths through the pipeline, depending
# on the state of the data. The pipeline software does not enforce
# any choices about the makeup or ordering of these stages; the user
# must be familiar enough with the pipeline to choose a path that makes
# sense.
# Other possible paths include:
# order   convolve, cull, fit, diags
# This is a typical secondpass run, using the multikernel option to
# regenerate templates using spatially varying transfer kernels
# generated by the dance, and omitting the transform stage because it's
# already been performed. The archive stage creates a 
# directory after the name of the LRI, with some specifications,
# and archives the products of both runs.
# order   convolve, transform, cull, fit, diags, dance, plotdance
# This is a typical GOODS ACS-to-lores firstpass run, using previously
# generated cutouts.
#------------------------- CUTOUT STAGE ----------------------------#
# *** hiresfile: the image from which the cutouts will be made,
# using the catalog and segmentation information in the other
# two files. 
# *** culling: if True, objects in the catalog but not
# falling into the lowres image will not be processed; if
# it is false, all objects in the catalog will be processed
# (useful for storing cutouts for future reuse on different
# datasets) and the selection of objects will be done before
# the convolution stage.
# *** hicatcolnames: if None, the default set of column names from the 
# input SEx catalog are used:
# NUMBER, X_IMAGE, Y_IMAGE, XMIN_IMAGE, YMIN_IMAGE, 
# XMAX_IMAGE, YMAX_IMAGE, BACKGROUND, FLUX_ISO
# If you need to specify a different set of names, be sure that
# the order corresponds to the default set. All names must be
# specified on the same line. Alternatively, you can provide
# a filename as the value here, with the actual column names
# in the file, all on the same line. 
# *** relscale: the relative pixel scale between the two images.
# For example if the hires images has a pixel scale of 0.1
# arcsec/pixel, and the lores image has a pixel scale of 0.5
# arcsec/pixel, the value of relscale should be 5. If LRI has been 
# swarped, relscale = 1.
# *** cutoutcat: the filename containing only those columns
# from the hirescat used by TPHOT (hicatcolnames).
# *** cutoutdir: the directory containing the cutouts.
# ---> Note that these are output parameters if you start from the 
# cutout stage; they are input parameters for the convolve stage.
# *** normalize: determines whether the cutouts will be
# normalized or not; it is normally set to true, so that the
# final output catalog will contain fluxes, rather than colors.
#-------------------------- CONVOLUTION STAGE -----------------------#
# *** loresfile, loreserr: the low resolution measure and rms images.
# TPHOT is designed to work with an RMS map as the error map,
# but it will also accept a weight map, or a single constant
# value of the RMS from which an RMS map is generated. The
# errtype specifies which kind of error image is provided.
# For best results, use a source-weighted RMS map, to prevent
# the bright objects from dominating the fit.
# *** bgconstant: global background constant can be subtracted from the
# lores image before fitting. At present this is not recommended; 
# instead, it is recommended that you background-correct the lores
# image as perfectly as possible before fitting.
# *** loresflag: a flag map is used at several points in the 
# pipeline to avoid constructing templates, and fitting pixels, 
# that contain bad data. At present this is implemented as a 
# maximum value: templates containing at least one pixel with a 
# flag map value greater than maxflag will not be created. 
# Eventually we may change this to work as a mask.
# *** Parameters relating to the transfer kernel:
# The transfer kernel must be a FITS image on the same
# pixel scale as the high res image. It should contain
# a centered, normalized image T such that
# hires PSF convolved with T = lores PSF.
# FFTconv is True if the convolution of cutouts with
# the smoothing kernel is to be done in Fourier space (FFTW3).
# kerntxt may be explicitely put to True if one wishes to 
# use a text file containing
# the kernel instead of the FITS one (TPHOT can obtain
# the text file from the fits one if necessary).
# TPHOT supports the use of multiple kernels to accommodate
# a spatially varying PSF. To use this option, set the
# multikernels value to true, and provide a kernellookup file
# that divides the lores image into rectangular zones,
# specified as pixel ranges, and provides a transfer kernel
# filename for each zone. Any objects which fall in a zone
# not included in the lookup file will use the transfer kernel
# specified as kernelfile.
# The posframe parameter determines whether the pixel ranges
# specified in the kernellookup files are in lores or hires pixels,
# and, accordingly, which catalog file will be used as an input
# to this stage:
#  - if posframe = lores, the lrfcat will be used. This is
# the normal mode for pass 2 processing.
#  - if posframe = hires, the cutoutcat will be used. This is the
# mode used when using multiple kernels in the first pass run.
# *** templatedir: the directory containing the templates created
# in the convolve stage, listed in the catalog templatecat.
# ---> Note that these are output parameters for the convolve stage,
# and an input parameter for all subsequent stages.
# The next two parameters control the optional production of
# hr templates: ie, templates that have been convolved with the
# kernel, but not binned down to the LR pixel resolution.
# If these images are created, they may (soon) be used by the
# compl_convolve stage, to apply a shift computed by the dance
# rather than re-convolving. They may also be used to construct
# an "hr collage" as a diagnostic product for use in constructing
# a better kernel.
#--------------------------- FITTING STAGE ------------------------------#
# *** Filenames:
# These are all output parameters. The "tfitpars" file
# specifies the name of the special parameter file for the fitting
# stage that will be generated from the parameters in this file.
# The others are filenames for the output catalog, cell, and
# covariance files, respectively.
# *** Control parameters:
# These are the most important control parameters.
# cell_xdim and cell_ydim specify the original size of the cell,
# in lores pixels. It should be large enough to contain 2-3 lores
# PSFs. The cell will grow to avoid chopping off partial templates,
# so this is a minimum size.
# However, in TPHOT the suggested options are:
# cell_xdim=cell_ydim=0 (cells-on-objects) or
# cell_xdim=cell_ydim<0 (single fit on whole image).
# The cell_overlap is also specified in lores pixels.
# dithercell=true dithers the pattern of overlapping cells over
#                  the lores image, to avoid corner problems.
# cellmask=true   uses a mask to exclude pixels from the fit which
#                  do not contain a value of at least "maskfloor" in
#                  at least one template. This is necessary because
#                  of the cell growth.
# maskfloor=1e9   should be set to a value at which it's okay to clip
#                  the templates.
# fitbackground=false  does not fit the background in each cell. This
#                  is necessary because of the cell growth.
# writecovar=true writes the covariance information out to the covar
#                  file.
# *** Options:
# Here the different possibilities for solving the linear system 
# are given. If "CONVPHOT" is True, the matrix is built 
# using the Convphot method, otherwise the old TFIT method
# is adopted. "Threshold" is only considered if CONVPHOT is true
# and is used to include the possibility to use a threshold on
# the flux to only use the central parts of the objects.
# "LINSYS_SOLVER" gives the chosen soltion method: LU, Cholesky 
# or Iterative Biconjugate Gradient (IBG) method. LU is default.
# "Clip" tells whether to loop on the sources escluding negative
# solutions.
# STANDARD TFIT MODE: CONVPHOT=False, Threshold=0.0, SOLVER=lu, Clip=False
#----------------------- DIAGNOSTICS STAGES -----------------------------#
# *** modelfile: the FITS file that will contain the collage made by
# multiplying each template by its best flux and dropping it into the
# right place. An additional diagnostic file will be created: it will
# contain the difference image (loresimage - modelfile). Its filename
# will be created by prepending "resid_" to the modelfile.
# *** Dance:
# dzonesize specifies the size of the rectangular zones over which a
# shift will be calculated. It should be comparable to the size over
# which the misregistration is roughly constant; but, it must be
# large enough to contain enough objects to provide a good signal
# to the crosscorrelation.
# maxshift specifies the maximum size of the x/y shift, in lores
# pixels, which is considered to be valid. Any shift larger than this
# is considered spurious and dropped from the final results, and
# replaced by an interpolated value from the surrounding zones.
# ddiagfile is an output parameter for the dance stage, and an input
# parameter for the plotdance stage.
# dlogfile is an output parameter; it simply contains the output from
# the crosscorrelation process.
# danceFFT is true if the dance correlation is to be performed
# using FFT techniques rather than in real pixels space.
#
#--------------------------- OLDER STUFF -------------------------------#
#Params relating to the transform stage
lrfcat          hrc_trim.txt_lrf

#This is the "cutoutcat" file, transformed into the
#pixel reference frame of the lores image. It is an output
#parameter for the transform stage, and an input parameter
#for the cull stage.
#..................................................................
#..................................................................

#Params relating to the cull stage
#templatecat     templates.cat

#This is the "lrfcat" file, modified to exclude or modify objects
#that were culled because their templates fell entirely or
#partially outside the bounds of the lores image. It is an
#output parameter for the cull stage, and an input parameter
#for all subsequent stages.
#..................................................................
#..................................................................
